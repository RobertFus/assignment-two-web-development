"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var mongoose_1 = __importDefault(require("mongoose"));
var User_1 = require("./persistance/User");
var StrongParams_1 = require("./middleware/StrongParams");
var ServiceType_1 = require("./persistance/ServiceType");
var ReviewContractor_1 = require("./persistance/ReviewContractor");
var cookie_parser_1 = __importDefault(require("cookie-parser"));
var SessionAuth_1 = require("./middleware/SessionAuth");
var express_graphql_1 = __importDefault(require("express-graphql"));
var GraphQlSchema_1 = __importDefault(require("./graph-ql/GraphQlSchema"));
var app = express_1.default();
var port = 5000;
app.use(express_1.default.json());
app.use(cookie_parser_1.default('Secret'));
mongoose_1.default.connect('mongodb://localhost/test');
app.post('/user', [
    StrongParams_1.strongParams({
        userName: 'string',
        firstName: 'string',
        lastName: 'string',
        password: 'string',
        userAddress: 'string',
        userCity: 'string',
        userState: 'string',
        userZipcode: 'string',
        userType: 'string',
        userId: 'number',
        sessionId: 'string'
    })
], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, userName, firstName, lastName, password, userAddress, userCity, userState, userZipcode, userType, userId, sessionId, user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                strongParams = response.locals.strongParams;
                userName = strongParams.userName;
                firstName = strongParams.firstName;
                lastName = strongParams.lastName;
                password = strongParams.password;
                userAddress = strongParams.userAddress;
                userCity = strongParams.userCity;
                userState = strongParams.userState;
                userZipcode = strongParams.userZipcode;
                userType = strongParams.userType;
                userId = strongParams.userId;
                sessionId = strongParams.sessionId;
                return [4 /*yield*/, User_1.User.create({
                        userName: userName,
                        firstName: firstName,
                        lastName: lastName,
                        password: password,
                        userAddress: userAddress,
                        userCity: userCity,
                        userState: userState,
                        userZipcode: userZipcode,
                        userType: userType,
                        userId: userId,
                        sessionId: sessionId
                    })];
            case 1:
                user = _a.sent();
                response.send(user);
                return [2 /*return*/];
        }
    });
}); });
//Get requests for user, Searches for one user.
//----------------------------------------------------------------------------------------------------------------------
app.get('/user', [SessionAuth_1.SessionAuth({ requireCookie: true })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var userName, firstName, lastName, password, userAddress, userCity, userState, userZipcode, userType, userId, sessionId, user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                userName = request.body.userName;
                firstName = request.body.firstName;
                lastName = request.body.lastName;
                password = request.body.password;
                userAddress = request.body.userAddress;
                userCity = request.body.userCity;
                userState = request.body.userState;
                userZipcode = request.body.userZipcode;
                userType = request.body.userType;
                userId = request.body.userId;
                sessionId = request.body.sessionId;
                return [4 /*yield*/, User_1.User.findOne({
                        userName: userName,
                        firstName: firstName,
                        lastName: lastName,
                        password: password,
                        userAddress: userAddress,
                        userCity: userCity,
                        userState: userState,
                        userZipcode: userZipcode,
                        userType: userType,
                        userId: userId,
                        sessionId: sessionId
                    })];
            case 1:
                user = _a.sent();
                return [2 /*return*/, response.send(user)];
        }
    });
}); });
//Delete request for users, will delete one user when requested
//----------------------------------------------------------------------------------------------------------------------
app.delete('/user', [SessionAuth_1.SessionAuth({ requireCookie: true })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var userName, firstName, lastName, password, userAddress, userCity, userState, userZipcode, userType, userId, sessionId, user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                userName = request.body.userName;
                firstName = request.body.firstName;
                lastName = request.body.lastName;
                password = request.body.password;
                userAddress = request.body.userAddress;
                userCity = request.body.userCity;
                userState = request.body.userState;
                userZipcode = request.body.userZipcode;
                userType = request.body.userType;
                userId = request.body.userId;
                sessionId = request.body.sessionId;
                return [4 /*yield*/, User_1.User.deleteOne({
                        userName: userName,
                        firstName: firstName,
                        lastName: lastName,
                        password: password,
                        userAddress: userAddress,
                        userCity: userCity,
                        userState: userState,
                        userZipcode: userZipcode,
                        userType: userType,
                        userId: userId,
                        sessionId: sessionId
                    })];
            case 1:
                user = _a.sent();
                return [2 /*return*/, response.send(user)];
        }
    });
}); });
//----------------------------------------------------------------------------------------------------------------------
app.post('/service', [StrongParams_1.strongParams({
        serviceId: 'number',
        snowService: 'string',
        grassService: 'string',
        lawnService: 'string',
        estimatedCost: 'number'
    })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, serviceId, snowService, grassService, lawnService, estimatedCost, service;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                strongParams = response.locals.strongParams;
                serviceId = strongParams.serviceId;
                snowService = strongParams.snowService;
                grassService = strongParams.grassService;
                lawnService = strongParams.lawnService;
                estimatedCost = strongParams.estimatedCost;
                return [4 /*yield*/, ServiceType_1.ServiceTypes.create({
                        serviceId: serviceId,
                        snowService: snowService,
                        grassService: grassService,
                        lawnService: lawnService,
                        estimatedCost: estimatedCost
                    })];
            case 1:
                service = _a.sent();
                response.send(service);
                return [2 /*return*/];
        }
    });
}); });
//----------------------------------------------------------------------------------------------------------------------
app.get('/service', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var serviceId, snowService, grassService, lawnService, estimatedCost, service;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                serviceId = request.body.serviceId;
                snowService = request.body.snowService;
                grassService = request.body.grassService;
                lawnService = request.body.lawnService;
                estimatedCost = request.body.estimatedCost;
                return [4 /*yield*/, ServiceType_1.ServiceTypes.findOne({
                        serviceId: serviceId,
                        snowService: snowService,
                        grassService: grassService,
                        lawnService: lawnService,
                        estimatedCost: estimatedCost
                    })];
            case 1:
                service = _a.sent();
                return [2 /*return*/, response.send(service)];
        }
    });
}); });
//----------------------------------------------------------------------------------------------------------------------
app.delete('/service', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var serviceId, snowService, grassService, lawnService, estimatedCost, service;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                serviceId = request.body.serviceId;
                snowService = request.body.snowService;
                grassService = request.body.grassService;
                lawnService = request.body.lawnService;
                estimatedCost = request.body.estimatedCost;
                return [4 /*yield*/, ServiceType_1.ServiceTypes.deleteOne({
                        serviceId: serviceId,
                        snowService: snowService,
                        grassService: grassService,
                        lawnService: lawnService,
                        estimatedCost: estimatedCost
                    })];
            case 1:
                service = _a.sent();
                return [2 /*return*/, response.send(service)];
        }
    });
}); });
//Post request for reviews
//----------------------------------------------------------------------------------------------------------------------
app.post('/review', [StrongParams_1.strongParams({
        userName: 'string',
        customerComment: 'string',
        customerRating: 'number',
        reviewId: 'number'
    })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongParams, userName, customerComment, customerRating, reviewId, review;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                strongParams = response.locals.strongParams;
                userName = strongParams.userName;
                customerComment = strongParams.customerComment;
                customerRating = strongParams.customerRating;
                reviewId = strongParams.reviewId;
                return [4 /*yield*/, ReviewContractor_1.ReviewContactor.create({
                        userName: userName,
                        customerComment: customerComment,
                        customerRating: customerRating,
                        reviewId: reviewId
                    })];
            case 1:
                review = _a.sent();
                response.send(review);
                return [2 /*return*/];
        }
    });
}); });
//Get request for reviews.
//----------------------------------------------------------------------------------------------------------------------
app.get('/review', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var userName, customerComment, customerRating, reviewId, review;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                userName = request.body.userName;
                customerComment = request.body.customerComment;
                customerRating = request.body.customerRating;
                reviewId = request.body.reviewId;
                return [4 /*yield*/, ReviewContractor_1.ReviewContactor.findOne({
                        userName: userName,
                        customerComment: customerComment,
                        customerRating: customerRating,
                        reviewId: reviewId
                    })];
            case 1:
                review = _a.sent();
                return [2 /*return*/, response.send(review)];
        }
    });
}); });
//Delete request for review contractor model.
//----------------------------------------------------------------------------------------------------------------------
app.delete('/review', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var userName, customerComment, customerRating, reviewId, review;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                userName = request.body.userName;
                customerComment = request.body.customerComment;
                customerRating = request.body.customerRating;
                reviewId = request.body.reviewId;
                return [4 /*yield*/, ReviewContractor_1.ReviewContactor.deleteOne({
                        userName: userName,
                        customerComment: customerComment,
                        customerRating: customerRating,
                        reviewId: reviewId
                    })];
            case 1:
                review = _a.sent();
                return [2 /*return*/, response.send(review)];
        }
    });
}); });
app.use('/graphql', express_graphql_1.default({
    schema: GraphQlSchema_1.default,
    graphiql: true
}));
/*
const webSocketServer: WebSocket.Server = new WebSocket.Server({ port });
webSocketServer.on('connection', (webSocketClient: WebSocket) => {
    console.log('A client has connected to the websocket server!');
    webSocketClient.send('Hello from the websocket server! You have successfully connected!');
    webSocketClient.on('message', (message) => {
        console.log('Received message from client: ', message);
        webSocketServer.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
                client.send('Broadcast message from the websocket server: ' + message);


            }
        });
        webSocketClient.on('message', (message) => {
            console.log('received %s', message);
        });
        console.log('Broadcasted message to all connected clients!');
    });
});

console.log('Websocket server is up and ready for connections on port', port);

 */
app.listen(4000, function () { return console.log('Server Up'); });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL1NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLG9EQUE0RDtBQUM1RCxzREFBZ0M7QUFDaEMsMkNBQXdDO0FBQ3hDLDBEQUF1RDtBQUN2RCx5REFBdUQ7QUFDdkQsbUVBQStEO0FBQy9ELGdFQUF5QztBQUN6Qyx3REFBcUQ7QUFDckQsb0VBQTBDO0FBQzFDLDJFQUFxRDtBQUVyRCxJQUFNLEdBQUcsR0FBRyxpQkFBTyxFQUFFLENBQUM7QUFDdEIsSUFBTSxJQUFJLEdBQVcsSUFBSSxDQUFDO0FBRzFCLEdBQUcsQ0FBQyxHQUFHLENBQUMsaUJBQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0FBQ3hCLEdBQUcsQ0FBQyxHQUFHLENBQUMsdUJBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0FBQ2hDLGtCQUFRLENBQUMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7QUFFN0MsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUc7SUFDZiwyQkFBWSxDQUFFO1FBQ1YsUUFBUSxFQUFFLFFBQVE7UUFDbEIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsV0FBVyxFQUFFLFFBQVE7UUFDckIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsU0FBUyxFQUFFLFFBQVE7UUFDbkIsV0FBVyxFQUFFLFFBQVE7UUFDckIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsTUFBTSxFQUFFLFFBQVE7UUFDaEIsU0FBUyxFQUFFLFFBQVE7S0FDdEIsQ0FBQztDQUNMLEVBQUcsVUFBTyxPQUFnQixFQUFFLFFBQWtCOzs7OztnQkFDckMsWUFBWSxHQUFRLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUVqRCxRQUFRLEdBQXVCLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JELFNBQVMsR0FBdUIsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDdkQsUUFBUSxHQUF1QixZQUFZLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxRQUFRLEdBQXVCLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JELFdBQVcsR0FBdUIsWUFBWSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsUUFBUSxHQUF1QixZQUFZLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxTQUFTLEdBQXVCLFlBQVksQ0FBQyxTQUFTLENBQUM7Z0JBQ3ZELFdBQVcsR0FBdUIsWUFBWSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsUUFBUSxHQUF1QixZQUFZLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxNQUFNLEdBQXVCLFlBQVksQ0FBQyxNQUFNLENBQUM7Z0JBQ2pELFNBQVMsR0FBdUIsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFFaEQscUJBQU0sV0FBSSxDQUFDLE1BQU0sQ0FBRTt3QkFDNUIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsU0FBUyxFQUFFLFNBQVM7d0JBQ3BCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsTUFBTSxFQUFFLE1BQU07d0JBQ2QsU0FBUyxFQUFFLFNBQVM7cUJBQ3ZCLENBQUMsRUFBQTs7Z0JBWkksSUFBSSxHQUFHLFNBWVg7Z0JBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7OztLQUNuQixDQUFDLENBQUM7QUFHUCwrQ0FBK0M7QUFDL0Msd0hBQXdIO0FBQ3hILEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMseUJBQVcsQ0FBQyxFQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDLEVBQUcsVUFBTyxPQUFnQixFQUFFLFFBQWtCOzs7OztnQkFFekYsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsU0FBUyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDdkQsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsV0FBVyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsU0FBUyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDdkQsV0FBVyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsTUFBTSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDakQsU0FBUyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFFaEQscUJBQU0sV0FBSSxDQUFDLE9BQU8sQ0FBRTt3QkFDN0IsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsU0FBUyxFQUFFLFNBQVM7d0JBQ3BCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsTUFBTSxFQUFFLE1BQU07d0JBQ2QsU0FBUyxFQUFFLFNBQVM7cUJBQ3ZCLENBQUMsRUFBQTs7Z0JBWkksSUFBSSxHQUFHLFNBWVg7Z0JBQ0Ysc0JBQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBQzs7O0tBRTlCLENBQUMsQ0FBQztBQUVILCtEQUErRDtBQUMvRCx3SEFBd0g7QUFDeEgsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyx5QkFBVyxDQUFDLEVBQUMsYUFBYSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsRUFBRyxVQUFPLE9BQWdCLEVBQUUsUUFBa0I7Ozs7O2dCQUU1RixRQUFRLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxTQUFTLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUN2RCxRQUFRLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxRQUFRLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxXQUFXLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUMzRCxRQUFRLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxTQUFTLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUN2RCxXQUFXLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUMzRCxRQUFRLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNyRCxNQUFNLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUNqRCxTQUFTLEdBQXVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO2dCQUVoRCxxQkFBTSxXQUFJLENBQUMsU0FBUyxDQUFFO3dCQUMvQixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsU0FBUyxFQUFFLFNBQVM7d0JBQ3BCLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixRQUFRLEVBQUUsUUFBUTt3QkFDbEIsV0FBVyxFQUFFLFdBQVc7d0JBQ3hCLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixTQUFTLEVBQUUsU0FBUzt3QkFDcEIsV0FBVyxFQUFFLFdBQVc7d0JBQ3hCLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixNQUFNLEVBQUUsTUFBTTt3QkFDZCxTQUFTLEVBQUUsU0FBUztxQkFDdkIsQ0FBQyxFQUFBOztnQkFaSSxJQUFJLEdBQUcsU0FZWDtnQkFDRixzQkFBTyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFDOzs7S0FDOUIsQ0FBQyxDQUFDO0FBRUgsd0hBQXdIO0FBQ3hILEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUNmLENBQUUsMkJBQVksQ0FBRTtRQUNaLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFdBQVcsRUFBRSxRQUFRO1FBQ3JCLFlBQVksRUFBRSxRQUFRO1FBQ3RCLFdBQVcsRUFBRSxRQUFRO1FBQ3JCLGFBQWEsRUFBRSxRQUFRO0tBRTFCLENBQUMsQ0FBQyxFQUFHLFVBQU8sT0FBZ0IsRUFBRSxRQUFrQjs7Ozs7Z0JBRXZDLFlBQVksR0FBUSxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztnQkFFakQsU0FBUyxHQUF1QixZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUN2RCxXQUFXLEdBQXVCLFlBQVksQ0FBQyxXQUFXLENBQUM7Z0JBQzNELFlBQVksR0FBdUIsWUFBWSxDQUFDLFlBQVksQ0FBQztnQkFDN0QsV0FBVyxHQUF1QixZQUFZLENBQUMsV0FBVyxDQUFDO2dCQUMzRCxhQUFhLEdBQXVCLFlBQVksQ0FBQyxhQUFhLENBQUM7Z0JBQ3JELHFCQUFNLDBCQUFZLENBQUMsTUFBTSxDQUFDO3dCQUN0QyxTQUFTLEVBQUUsU0FBUzt3QkFDcEIsV0FBVyxFQUFFLFdBQVc7d0JBQ3hCLFlBQVksRUFBRSxZQUFZO3dCQUMxQixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsYUFBYSxFQUFFLGFBQWE7cUJBQy9CLENBQUMsRUFBQTs7Z0JBTkksT0FBTyxHQUFHLFNBTWQ7Z0JBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs7OztLQUUxQixDQUFDLENBQUM7QUFFUCx3SEFBd0g7QUFDeEgsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsVUFBTyxPQUFnQixFQUFFLFFBQWtCOzs7OztnQkFDckQsU0FBUyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDdkQsV0FBVyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsWUFBWSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDN0QsV0FBVyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDM0QsYUFBYSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztnQkFDckQscUJBQU0sMEJBQVksQ0FBQyxPQUFPLENBQUM7d0JBQ3ZDLFNBQVMsRUFBRSxTQUFTO3dCQUNwQixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsWUFBWSxFQUFFLFlBQVk7d0JBQzFCLFdBQVcsRUFBRSxXQUFXO3dCQUN4QixhQUFhLEVBQUUsYUFBYTtxQkFDL0IsQ0FBQyxFQUFBOztnQkFOSSxPQUFPLEdBQUcsU0FNZDtnQkFDRixzQkFBTyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFDOzs7S0FDakMsQ0FBQyxDQUFDO0FBRUgsd0hBQXdIO0FBQ3hILEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLFVBQU8sT0FBZ0IsRUFBRSxRQUFrQjs7Ozs7Z0JBQ3hELFNBQVMsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ3ZELFdBQVcsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQzNELFlBQVksR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQzdELFdBQVcsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQzNELGFBQWEsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQ3JELHFCQUFNLDBCQUFZLENBQUMsU0FBUyxDQUFDO3dCQUN6QyxTQUFTLEVBQUUsU0FBUzt3QkFDcEIsV0FBVyxFQUFFLFdBQVc7d0JBQ3hCLFlBQVksRUFBRSxZQUFZO3dCQUMxQixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsYUFBYSxFQUFFLGFBQWE7cUJBRS9CLENBQUMsRUFBQTs7Z0JBUEksT0FBTyxHQUFHLFNBT2Q7Z0JBQ0Ysc0JBQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBQzs7O0tBQ2pDLENBQUMsQ0FBQztBQUNILDBCQUEwQjtBQUMxQix3SEFBd0g7QUFDeEgsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQ2QsQ0FBQywyQkFBWSxDQUFFO1FBQ1gsUUFBUSxFQUFFLFFBQVE7UUFDbEIsZUFBZSxFQUFFLFFBQVE7UUFDekIsY0FBYyxFQUFFLFFBQVE7UUFDeEIsUUFBUSxFQUFFLFFBQVE7S0FDckIsQ0FBQyxDQUFDLEVBQUcsVUFBTyxPQUFnQixFQUFFLFFBQWtCOzs7OztnQkFFdkMsWUFBWSxHQUFRLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO2dCQUNqRCxRQUFRLEdBQXVCLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JELGVBQWUsR0FBdUIsWUFBWSxDQUFDLGVBQWUsQ0FBQztnQkFDbkUsY0FBYyxHQUF1QixZQUFZLENBQUMsY0FBYyxDQUFDO2dCQUNqRSxRQUFRLEdBQXVCLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBQzVDLHFCQUFNLGtDQUFlLENBQUMsTUFBTSxDQUFDO3dCQUN4QyxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsZUFBZSxFQUFFLGVBQWU7d0JBQ2hDLGNBQWMsRUFBRSxjQUFjO3dCQUM5QixRQUFRLEVBQUUsUUFBUTtxQkFDckIsQ0FBQyxFQUFBOztnQkFMSSxNQUFNLEdBQUcsU0FLYjtnQkFDRixRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzs7O0tBQ3pCLENBQUMsQ0FBQztBQUNQLDBCQUEwQjtBQUMxQix3SEFBd0g7QUFDeEgsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsVUFBTyxPQUFnQixFQUFFLFFBQWtCOzs7OztnQkFFcEQsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDckQsZUFBZSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztnQkFDbkUsY0FBYyxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDakUsUUFBUSxHQUF1QixPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDNUMscUJBQU0sa0NBQWUsQ0FBQyxPQUFPLENBQUM7d0JBQ3pDLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixlQUFlLEVBQUUsZUFBZTt3QkFDaEMsY0FBYyxFQUFFLGNBQWM7d0JBQzlCLFFBQVEsRUFBRSxRQUFRO3FCQUNyQixDQUFDLEVBQUE7O2dCQUxJLE1BQU0sR0FBRyxTQUtiO2dCQUNGLHNCQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUM7OztLQUVoQyxDQUFDLENBQUM7QUFDSCw2Q0FBNkM7QUFDN0Msd0hBQXdIO0FBQ3hILEdBQUcsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFVBQU8sT0FBZ0IsRUFBRSxRQUFrQjs7Ozs7Z0JBRXZELFFBQVEsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3JELGVBQWUsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7Z0JBQ25FLGNBQWMsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQ2pFLFFBQVEsR0FBdUIsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzVDLHFCQUFNLGtDQUFlLENBQUMsU0FBUyxDQUFDO3dCQUMzQyxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsZUFBZSxFQUFFLGVBQWU7d0JBQ2hDLGNBQWMsRUFBRSxjQUFjO3dCQUM5QixRQUFRLEVBQUUsUUFBUTtxQkFDckIsQ0FBQyxFQUFBOztnQkFMSSxNQUFNLEdBQUcsU0FLYjtnQkFDRixzQkFBTyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFDOzs7S0FDaEMsQ0FBQyxDQUFDO0FBRUgsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUseUJBQVcsQ0FBQztJQUM1QixNQUFNLEVBQUUsdUJBQWE7SUFDckIsUUFBUSxFQUFFLElBQUk7Q0FFakIsQ0FBQyxDQUFDLENBQUM7QUFFSjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0F1Qkc7QUFHSCxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDIn0=