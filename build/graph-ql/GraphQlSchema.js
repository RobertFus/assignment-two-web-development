"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var RootQuery_1 = __importDefault(require("./queries/RootQuery"));
var RootMutation_1 = __importDefault(require("./mutations/RootMutation"));
exports.default = new graphql_1.GraphQLSchema({
    query: RootQuery_1.default,
    mutation: RootMutation_1.default,
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JhcGhRbFNjaGVtYS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ncmFwaC1xbC9HcmFwaFFsU2NoZW1hLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsbUNBQXNDO0FBQ3RDLGtFQUE0QztBQUM1QywwRUFBb0Q7QUFFcEQsa0JBQWUsSUFBSSx1QkFBYSxDQUFDO0lBRTdCLEtBQUssRUFBRSxtQkFBUztJQUNoQixRQUFRLEVBQUUsc0JBQVk7Q0FFekIsQ0FBQyxDQUFDIn0=