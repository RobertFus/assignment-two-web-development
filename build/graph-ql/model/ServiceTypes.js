"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.GraphQlService = new graphql_1.GraphQLObjectType({
    name: 'Service',
    description: 'Description of a service',
    fields: function () { return ({
        serviceId: { type: graphql_1.GraphQLString },
        snowService: { type: graphql_1.GraphQLString },
        grassService: { type: graphql_1.GraphQLString },
        lawnService: { type: graphql_1.GraphQLString },
        estimatedCost: { type: graphql_1.GraphQLString },
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZVR5cGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2dyYXBoLXFsL21vZGVsL1NlcnZpY2VUeXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLG1DQUF5RDtBQUU1QyxRQUFBLGNBQWMsR0FBRyxJQUFJLDJCQUFpQixDQUFDO0lBQ2hELElBQUksRUFBRSxTQUFTO0lBQ2YsV0FBVyxFQUFFLDBCQUEwQjtJQUN2QyxNQUFNLEVBQUUsY0FBTSxPQUFBLENBQUM7UUFDWCxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNsQyxXQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNwQyxZQUFZLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNyQyxXQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNwQyxhQUFhLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtLQUN6QyxDQUFDLEVBTlksQ0FNWjtDQUNMLENBQUMsQ0FBQyJ9