"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.ServiceInput = new graphql_1.GraphQLInputObjectType({
    name: 'Service',
    description: 'Description of a service',
    fields: function () { return ({
        serviceId: { type: graphql_1.GraphQLString },
        snowService: { type: graphql_1.GraphQLString },
        grassService: { type: graphql_1.GraphQLString },
        lawnService: { type: graphql_1.GraphQLString },
        estimatedCost: { type: graphql_1.GraphQLString },
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZUlucHV0cy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9ncmFwaC1xbC9tb2RlbC9zZXJ2aWNlL1NlcnZpY2VJbnB1dHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtQ0FBOEQ7QUFFakQsUUFBQSxZQUFZLEdBQUcsSUFBSSxnQ0FBc0IsQ0FBQztJQUNuRCxJQUFJLEVBQUUsU0FBUztJQUNmLFdBQVcsRUFBRSwwQkFBMEI7SUFDdkMsTUFBTSxFQUFFLGNBQU0sT0FBQSxDQUFDO1FBQ1gsU0FBUyxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7UUFDbEMsV0FBVyxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7UUFDcEMsWUFBWSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7UUFDckMsV0FBVyxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7UUFDcEMsYUFBYSxFQUFFLEVBQUUsSUFBSSxFQUFFLHVCQUFhLEVBQUU7S0FDekMsQ0FBQyxFQU5ZLENBTVo7Q0FDTCxDQUFDLENBQUMifQ==