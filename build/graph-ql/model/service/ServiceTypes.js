"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var ServiceType_1 = require("../../../persistance/ServiceType");
exports.GraphQlService = new graphql_1.GraphQLObjectType({
    name: 'ServiceInput',
    description: 'Description of a service',
    fields: function () { return ({
        serviceId: { type: graphql_1.GraphQLString },
        snowService: { type: graphql_1.GraphQLString },
        grassService: { type: graphql_1.GraphQLString },
        lawnService: { type: graphql_1.GraphQLString },
        estimatedCost: {
            type: graphql_1.GraphQLList(exports.GraphQlService),
            resolve: function (parent, args) {
                return ServiceType_1.ServiceTypes.find({ estimatedCost: { $in: parent.lawnService } });
            }
        }
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZVR5cGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2dyYXBoLXFsL21vZGVsL3NlcnZpY2UvU2VydmljZVR5cGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUNBQXNFO0FBQ3RFLGdFQUE4RDtBQUVqRCxRQUFBLGNBQWMsR0FBZ0MsSUFBSSwyQkFBaUIsQ0FBQztJQUM3RSxJQUFJLEVBQUUsY0FBYztJQUNwQixXQUFXLEVBQUUsMEJBQTBCO0lBQ3ZDLE1BQU0sRUFBRSxjQUFNLE9BQUEsQ0FBQztRQUNYLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2xDLFdBQVcsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ3BDLFlBQVksRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ3JDLFdBQVcsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ3BDLGFBQWEsRUFBRTtZQUNYLElBQUksRUFBRSxxQkFBVyxDQUFDLHNCQUFjLENBQUM7WUFDakMsT0FBTyxZQUFDLE1BQU0sRUFBRSxJQUFJO2dCQUNoQixPQUFPLDBCQUFZLENBQUMsSUFBSSxDQUFDLEVBQUMsYUFBYSxFQUFFLEVBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUMsRUFBQyxDQUFDLENBQUM7WUFDekUsQ0FBQztTQUNKO0tBQ0osQ0FBQyxFQVhZLENBV1o7Q0FDTCxDQUFDLENBQUMifQ==