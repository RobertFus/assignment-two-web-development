"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.GraphQlUser = new graphql_1.GraphQLObjectType({
    name: 'User',
    description: 'User Account',
    fields: function () { return ({
        userName: { type: graphql_1.GraphQLString },
        firstName: { type: graphql_1.GraphQLString },
        lastName: { type: graphql_1.GraphQLString },
        password: { type: graphql_1.GraphQLString },
        userAddress: { type: graphql_1.GraphQLString },
        userCity: { type: graphql_1.GraphQLString },
        userState: { type: graphql_1.GraphQLString },
        userZipcode: { type: graphql_1.GraphQLString },
        userType: { type: graphql_1.GraphQLString },
        userId: { type: graphql_1.GraphQLString },
        sessionId: { type: graphql_1.GraphQLString },
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9ncmFwaC1xbC9tb2RlbC91c2VyL1VzZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtQ0FBeUQ7QUFFNUMsUUFBQSxXQUFXLEdBQUcsSUFBSSwyQkFBaUIsQ0FBQztJQUM3QyxJQUFJLEVBQUUsTUFBTTtJQUNaLFdBQVcsRUFBRSxjQUFjO0lBQzNCLE1BQU0sRUFBRSxjQUFNLE9BQUEsQ0FBQztRQUNYLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2pDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2xDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2pDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2pDLFdBQVcsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ3BDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2pDLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2xDLFdBQVcsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ3BDLFFBQVEsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQ2pDLE1BQU0sRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO1FBQy9CLFNBQVMsRUFBRSxFQUFFLElBQUksRUFBRSx1QkFBYSxFQUFFO0tBQ3JDLENBQUMsRUFaWSxDQVlaO0NBQ0wsQ0FBQyxDQUFDIn0=