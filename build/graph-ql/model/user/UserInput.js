"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
exports.UserInput = new graphql_1.GraphQLInputObjectType({
    name: 'UserInput',
    description: 'Input fields for the User model',
    fields: function () { return ({
        userName: { type: graphql_1.GraphQLString },
        firstName: { type: graphql_1.GraphQLString },
        lastName: { type: graphql_1.GraphQLString },
        password: { type: graphql_1.GraphQLString },
        userAddress: { type: graphql_1.GraphQLString },
        userCity: { type: graphql_1.GraphQLString },
        userState: { type: graphql_1.GraphQLString },
        userZipcode: { type: graphql_1.GraphQLString },
        userType: { type: graphql_1.GraphQLString },
        userId: { type: graphql_1.GraphQLString },
        sessionId: { type: graphql_1.GraphQLString },
    }); },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlcklucHV0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2dyYXBoLXFsL21vZGVsL3VzZXIvVXNlcklucHV0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUNBQThEO0FBRWpELFFBQUEsU0FBUyxHQUFHLElBQUksZ0NBQXNCLENBQUM7SUFFaEQsSUFBSSxFQUFFLFdBQVc7SUFDakIsV0FBVyxFQUFFLGlDQUFpQztJQUU5QyxNQUFNLEVBQUUsY0FBTSxPQUFBLENBQUM7UUFDWCxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNqQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNsQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNqQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNqQyxXQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNwQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNqQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNsQyxXQUFXLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNwQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUNqQyxNQUFNLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtRQUMvQixTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsdUJBQWEsRUFBRTtLQUNyQyxDQUFDLEVBWlksQ0FZWjtDQUNMLENBQUMsQ0FBQyJ9