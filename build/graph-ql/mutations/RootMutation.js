"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var User_1 = require("../../persistance/User");
var ServiceType_1 = require("../../persistance/ServiceType");
var User_2 = require("../model/user/User");
var UserInput_1 = require("../model/user/UserInput");
var ServiceTypes_1 = require("../model/service/ServiceTypes");
var ServiceInputs_1 = require("../model/service/ServiceInputs");
exports.default = new graphql_1.GraphQLObjectType({
    name: 'RootMutations',
    description: 'Root level mutator resolvers',
    fields: function () { return ({
        createUser: {
            type: User_2.GraphQlUser,
            description: 'Creates a new user',
            args: {
                input: { type: UserInput_1.UserInput }
            },
            resolve: function (parentValue, _a) {
                var _b = _a.input, userName = _b.userName, firstName = _b.firstName, lastName = _b.lastName, password = _b.password, userAddress = _b.userAddress, userCity = _b.userCity, userState = _b.userState, userZipcode = _b.userZipcode, userType = _b.userType, userId = _b.userId, sessionId = _b.sessionId;
                return User_1.User.create({ userName: userName, firstName: firstName, lastName: lastName, password: password, userAddress: userAddress, userCity: userCity, userState: userState, userZipcode: userZipcode, userType: userType, userId: userId, sessionId: sessionId });
            },
        },
        createService: {
            type: ServiceTypes_1.GraphQlService,
            description: 'Creates service inputs',
            args: {
                input: { type: ServiceInputs_1.ServiceInput }
            },
            resolve: function (parentValue, _a) {
                var _b = _a.input, serviceId = _b.serviceId, snowService = _b.snowService, grassService = _b.grassService, lawnService = _b.lawnService, estimatedCost = _b.estimatedCost;
                return ServiceType_1.ServiceTypes.create({ serviceId: serviceId, snowService: snowService, grassService: grassService, lawnService: lawnService, estimatedCost: estimatedCost });
            },
        },
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUm9vdE11dGF0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2dyYXBoLXFsL211dGF0aW9ucy9Sb290TXV0YXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxtQ0FBMEM7QUFDMUMsK0NBQTRDO0FBQzVDLDZEQUEyRDtBQUMzRCwyQ0FBK0M7QUFDL0MscURBQWtEO0FBQ2xELDhEQUErRDtBQUMvRCxnRUFBOEQ7QUFFOUQsa0JBQWUsSUFBSSwyQkFBaUIsQ0FBQztJQUVqQyxJQUFJLEVBQUUsZUFBZTtJQUNyQixXQUFXLEVBQUUsOEJBQThCO0lBRTNDLE1BQU0sRUFBRSxjQUFNLE9BQUEsQ0FBQztRQUNYLFVBQVUsRUFBRTtZQUNSLElBQUksRUFBRSxrQkFBVztZQUNqQixXQUFXLEVBQUUsb0JBQW9CO1lBQ2pDLElBQUksRUFBRTtnQkFDRixLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUscUJBQVMsRUFBRTthQUM3QjtZQUNELE9BQU8sRUFBRSxVQUFDLFdBQWdCLEVBQUUsRUFBaUk7b0JBQWhJLGFBQThILEVBQXBILHNCQUFRLEVBQUUsd0JBQVMsRUFBRSxzQkFBUSxFQUFFLHNCQUFRLEVBQUUsNEJBQVcsRUFBRSxzQkFBUSxFQUFFLHdCQUFTLEVBQUUsNEJBQVcsRUFBRSxzQkFBUSxFQUFFLGtCQUFNLEVBQUUsd0JBQVM7Z0JBQ3RKLE9BQU8sV0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLFFBQVEsVUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLFdBQVcsYUFBQSxFQUFFLFFBQVEsVUFBQSxFQUFFLE1BQU0sUUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FBQztZQUVoSixDQUFDO1NBQ0o7UUFDRCxhQUFhLEVBQUU7WUFDWCxJQUFJLEVBQUUsNkJBQWM7WUFDcEIsV0FBVyxFQUFFLHdCQUF3QjtZQUNyQyxJQUFJLEVBQUU7Z0JBQ0YsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLDRCQUFZLEVBQUU7YUFDaEM7WUFDRCxPQUFPLEVBQUUsVUFBQyxXQUFnQixFQUFFLEVBQThFO29CQUE3RSxhQUEyRSxFQUFsRSx3QkFBUyxFQUFFLDRCQUFXLEVBQUUsOEJBQVksRUFBRSw0QkFBVyxFQUFFLGdDQUFhO2dCQUNsRyxPQUFPLDBCQUFZLENBQUMsTUFBTSxDQUFDLEVBQUMsU0FBUyxXQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsWUFBWSxjQUFBLEVBQUUsV0FBVyxhQUFBLEVBQUUsYUFBYSxlQUFBLEVBQUUsQ0FBQyxDQUFDO1lBRXBHLENBQUM7U0FDSjtLQUVKLENBQUMsRUF4QlksQ0F3Qlo7Q0FDTCxDQUFDLENBQUMifQ==