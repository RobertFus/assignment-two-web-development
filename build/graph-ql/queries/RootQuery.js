"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_1 = require("graphql");
var User_1 = require("../../persistance/User");
var ServiceType_1 = require("../../persistance/ServiceType");
var User_2 = require("../model/user/User");
var ServiceTypes_1 = require("../model/service/ServiceTypes");
exports.default = new graphql_1.GraphQLObjectType({
    name: 'RootQuery',
    description: 'Root level query resolvers',
    fields: function () { return ({
        users: {
            type: new graphql_1.GraphQLList(User_2.GraphQlUser),
            resolve: function (parentValue, args) {
                return User_1.User.findOne({ $or: [{ userName: args.userName }] });
            }
        },
        services: {
            type: new graphql_1.GraphQLList(ServiceTypes_1.GraphQlService),
            resolve: function (parentValue, args) {
                return ServiceType_1.ServiceTypes.find({});
            }
        }
    }); }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUm9vdFF1ZXJ5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2dyYXBoLXFsL3F1ZXJpZXMvUm9vdFF1ZXJ5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsbUNBQXdEO0FBQ3hELCtDQUE4QztBQUM5Qyw2REFBMkQ7QUFFM0QsMkNBQWlEO0FBQ2pELDhEQUE2RDtBQUU3RCxrQkFBZSxJQUFJLDJCQUFpQixDQUFDO0lBQ2pDLElBQUksRUFBRSxXQUFXO0lBQ2pCLFdBQVcsRUFBRSw0QkFBNEI7SUFDekMsTUFBTSxFQUFFLGNBQU0sT0FBQSxDQUFDO1FBQ1gsS0FBSyxFQUFFO1lBQ0gsSUFBSSxFQUFFLElBQUkscUJBQVcsQ0FBQyxrQkFBVyxDQUFDO1lBQ2xDLE9BQU8sWUFBQyxXQUFXLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxXQUFJLENBQUMsT0FBTyxDQUFFLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO1lBQ2hFLENBQUM7U0FDSjtRQUNELFFBQVEsRUFBRTtZQUNOLElBQUksRUFBRSxJQUFJLHFCQUFXLENBQUMsNkJBQWMsQ0FBQztZQUNyQyxPQUFPLFlBQUMsV0FBVyxFQUFFLElBQUk7Z0JBQ3JCLE9BQU8sMEJBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakMsQ0FBQztTQUNKO0tBQ0osQ0FBQyxFQWJZLENBYVo7Q0FDTCxDQUFDLENBQUMifQ==