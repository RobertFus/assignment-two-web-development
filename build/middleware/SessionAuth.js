"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User_1 = require("../persistance/User");
//Session Authentication to make sure that the user that is signed in is the current User
function SessionAuth(_a) {
    var _b = _a.requireCookie, requireCookie = _b === void 0 ? false : _b;
    return function (request, response, next) {
        if (request.signedCookies.sessionId) {
            var sessionId = request.signedCookies.sessionId;
            var currentSession = User_1.User.findOne({
                sessionId: sessionId
            });
            //Checks the current sessoin and sees if there is a session ID connected, if there is a session ID connected,
            // it moves forwards
            if (currentSession) {
                response.locals.sessionId = sessionId;
                return next();
            }
        }
        //Checks to cookie and if there is no cookie attached, it will send a 400 status
        if (requireCookie) {
            response.sendStatus(400);
            return next();
        }
    };
}
exports.SessionAuth = SessionAuth;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2Vzc2lvbkF1dGguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbWlkZGxld2FyZS9TZXNzaW9uQXV0aC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLDRDQUEwQztBQUMxQyx5RkFBeUY7QUFDekYsU0FBZ0IsV0FBVyxDQUFDLEVBQWtEO1FBQWhELHFCQUFxQixFQUFyQiwwQ0FBcUI7SUFDL0MsT0FBTSxVQUFDLE9BQWdCLEVBQUUsUUFBa0IsRUFBRSxJQUFrQjtRQUMvRCxJQUFHLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFO1lBQ2hDLElBQU0sU0FBUyxHQUFXLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1lBQzFELElBQU0sY0FBYyxHQUFHLFdBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ2hDLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUMsQ0FBQztZQUNILDZHQUE2RztZQUM3RyxvQkFBb0I7WUFDcEIsSUFBRyxjQUFjLEVBQUU7Z0JBQ2YsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2dCQUN0QyxPQUFPLElBQUksRUFBRSxDQUFDO2FBQ2pCO1NBQ0o7UUFDRCxnRkFBZ0Y7UUFDaEYsSUFBRyxhQUFhLEVBQUU7WUFDZCxRQUFRLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLE9BQU8sSUFBSSxFQUFFLENBQUM7U0FDakI7SUFDRCxDQUFDLENBQUE7QUFDTCxDQUFDO0FBcEJELGtDQW9CQyJ9