"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function strongParams(params) {
    return function (request, response, next) {
        var strongParams = {};
        var weakParams = request.body;
        Object.entries(params).forEach(function (_a) {
            var weakParamKey = _a[0], specifiedType = _a[1];
            var weakParam = weakParams[weakParamKey];
            if (weakParam && typeof weakParam === specifiedType) {
                strongParams[weakParamKey] = weakParam;
            }
        });
        response.locals.strongParams = strongParams;
        request.body = null;
        return next();
    };
}
exports.strongParams = strongParams;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3Ryb25nUGFyYW1zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL21pZGRsZXdhcmUvU3Ryb25nUGFyYW1zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsU0FBZ0IsWUFBWSxDQUFDLE1BQW1DO0lBQzVELE9BQU8sVUFBQyxPQUFnQixFQUFFLFFBQWtCLEVBQUUsSUFBa0I7UUFDNUQsSUFBTSxZQUFZLEdBQTRCLEVBQUUsQ0FBQTtRQUNoRCxJQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBRWhDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsRUFBNkI7Z0JBQTVCLG9CQUFZLEVBQUUscUJBQWE7WUFDeEQsSUFBTSxTQUFTLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNDLElBQUcsU0FBUyxJQUFJLE9BQU8sU0FBUyxLQUFLLGFBQWEsRUFBRTtnQkFDaEQsWUFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLFNBQVMsQ0FBQTthQUN6QztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQzVDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFDbEIsQ0FBQyxDQUFBO0FBQ0wsQ0FBQztBQWhCRCxvQ0FnQkMifQ==