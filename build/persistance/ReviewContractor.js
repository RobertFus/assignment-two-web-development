"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var reviewSchema = new mongoose_1.Schema({
    userName: {
        type: String,
        required: true,
        //Validation so that the user had a minimum length.
        validate: {
            validator: function (value) {
                return value.length > 6;
            },
            message: 'User name must contain more than 6 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    customerComment: {
        type: String,
        required: true,
        //Comments must have less than 300 characters so that there is not spam
        validate: {
            validator: function (value) {
                return value.length < 300;
            },
            message: 'Comment must be less than 300 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    customerRating: {
        type: Number,
        required: true,
        //Rating must be inbetween 1 -5 for a star rating.
        validate: {
            validator: function (value) {
                return (value > 0 && value < 6);
            },
            message: 'The rating must be between 1-5'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    reviewId: {
        type: Number,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The ID can not be negative'
        }
    }
});
exports.ReviewContactor = mongoose_1.model('review', reviewSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUmV2aWV3Q29udHJhY3Rvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9SZXZpZXdDb250cmFjdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBR3ZDLElBQU0sWUFBWSxHQUE4QixJQUFJLGlCQUFNLENBQXFCO0lBRTNFLFFBQVEsRUFBRTtRQUNOLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxtREFBbUQ7UUFDbkQsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUM1QixDQUFDO1lBQ0QsT0FBTyxFQUFFLCtDQUErQztTQUMzRDtLQUNKO0lBRUwsdUhBQXVIO0lBQ25ILGVBQWUsRUFBRTtRQUNiLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCx1RUFBdUU7UUFDdkUsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUM5QixDQUFDO1lBQ0QsT0FBTyxFQUFFLDBDQUEwQztTQUN0RDtLQUNKO0lBRUwsdUhBQXVIO0lBQ25ILGNBQWMsRUFBRTtRQUNaLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxrREFBa0Q7UUFDbEQsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLENBQUM7WUFDRCxPQUFPLEVBQUUsZ0NBQWdDO1NBQzVDO0tBQ0o7SUFFTCx1SEFBdUg7SUFDbkgsUUFBUSxFQUFFO1FBQ04sSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDckIsQ0FBQztZQUNELE9BQU8sRUFBRSw0QkFBNEI7U0FDeEM7S0FDSjtDQUNKLENBQUMsQ0FBQztBQUdVLFFBQUEsZUFBZSxHQUFHLGdCQUFLLENBQW9CLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQyJ9