"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
//---------------------------------------------------------------------------------------------------------------------
var serviceSchema = new mongoose_1.Schema({
    serviceId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The id must be greater than 0'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    snowService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    grassService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    lawnService: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    estimatedCost: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'The service cost must be greater than 0'
        }
    }
});
exports.ServiceTypes = mongoose_1.model('service', serviceSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VydmljZVR5cGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvcGVyc2lzdGFuY2UvU2VydmljZVR5cGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBdUM7QUFHdkMsdUhBQXVIO0FBQ3ZILElBQU0sYUFBYSxHQUEwQixJQUFJLGlCQUFNLENBQWlCO0lBQ3BFLFNBQVMsRUFBRTtRQUNQLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDckIsQ0FBQztZQUNELE9BQU8sRUFBRSwrQkFBK0I7U0FDM0M7S0FDSjtJQUNMLHVIQUF1SDtJQUNuSCxXQUFXLEVBQUU7UUFDVCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQy9FLENBQUM7WUFDRCxPQUFPLEVBQUUsaUNBQWlDO1NBQzdDO0tBQ0o7SUFFTCx1SEFBdUg7SUFDbkgsWUFBWSxFQUFFO1FBQ1YsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUNkLFFBQVEsRUFBRTtZQUNOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBQzlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUMvRSxDQUFDO1lBQ0QsT0FBTyxFQUFFLGlDQUFpQztTQUM3QztLQUNKO0lBRUwsdUhBQXVIO0lBQ25ILFdBQVcsRUFBRTtRQUNULElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7WUFDL0UsQ0FBQztZQUNELE9BQU8sRUFBRSxpQ0FBaUM7U0FDN0M7S0FDSjtJQUVMLHVIQUF1SDtJQUNuSCxhQUFhLEVBQUU7UUFDWCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ3JCLENBQUM7WUFDRCxPQUFPLEVBQUUseUNBQXlDO1NBQ3JEO0tBQ0o7Q0FFSixDQUFDLENBQUM7QUFHVSxRQUFBLFlBQVksR0FBRyxnQkFBSyxDQUFnQixTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUMifQ==