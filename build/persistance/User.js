"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    //---------------------------------------------------------------------------------------------------------------------
    userName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 6;
            },
            message: 'User name must contain more than 6 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Last Name must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    password: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z0-9!@#$%^&*]*$/.test(value);
            },
            message: 'Password must meet criteria a-z A-Z 0-9 !@#$%^&*'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    userAddress: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
        },
        message: 'User address must be larger than 0'
    },
    //---------------------------------------------------------------------------------------------------------------------
    userCity: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'City Must be a letter'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    userState: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'State must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    userZipcode: {
        type: String,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Zipcode must be 5 digits'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    userType: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Type must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    userId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'Id must be a number greater than zero.'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    sessionId: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Type must be a character'
        }
    }
});
exports.User = mongoose_1.model('user', userSchema);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVXNlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wZXJzaXN0YW5jZS9Vc2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXVDO0FBR3ZDLElBQU0sVUFBVSxHQUFrQixJQUFJLGlCQUFNLENBQVE7SUFFcEQsdUhBQXVIO0lBQ25ILFFBQVEsRUFBRTtRQUNOLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLENBQUM7WUFDRCxPQUFPLEVBQUUsK0NBQStDO1NBQzNEO0tBQ0o7SUFDRCx1SEFBdUg7SUFDdkgsU0FBUyxFQUFFO1FBQ1AsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUNkLFFBQVEsRUFBRTtZQUNOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBQzlCLE9BQU8sS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBQ0QsT0FBTyxFQUFFLGdDQUFnQztTQUM1QztLQUNKO0lBQ0QsdUhBQXVIO0lBQ3ZILFFBQVEsRUFBRTtRQUNOLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELE9BQU8sRUFBRSwrQkFBK0I7U0FDM0M7S0FDSjtJQUVMLHVIQUF1SDtJQUNuSCxRQUFRLEVBQUU7UUFDTixJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEUsQ0FBQztZQUNELE9BQU8sRUFBRSxrREFBa0Q7U0FDOUQ7S0FDSjtJQUVMLHVIQUF1SDtJQUNuSCxXQUFXLEVBQUU7UUFDVCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUM1QixDQUFDO1NBQ0o7UUFDRCxPQUFPLEVBQUUsb0NBQW9DO0tBQ2hEO0lBRUwsdUhBQXVIO0lBQ25ILFFBQVEsRUFBRTtRQUNOLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELE9BQU8sRUFBRSx1QkFBdUI7U0FDbkM7S0FDSjtJQUVMLHVIQUF1SDtJQUNuSCxTQUFTLEVBQUU7UUFDUCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFDRCxPQUFPLEVBQUUsMkJBQTJCO1NBQ3ZDO0tBQ0o7SUFFTCx1SEFBdUg7SUFDbkgsV0FBVyxFQUFFO1FBQ1QsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELE9BQU8sRUFBRSwwQkFBMEI7U0FDdEM7S0FDSjtJQUVMLHVIQUF1SDtJQUNuSCxRQUFRLEVBQUU7UUFDTixJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFO1lBQ04sU0FBUyxFQUFFLFVBQVUsS0FBYTtnQkFDOUIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELENBQUM7WUFDRCxPQUFPLEVBQUUsMEJBQTBCO1NBQ3RDO0tBQ0o7SUFFTCx1SEFBdUg7SUFDbkgsTUFBTSxFQUFFO1FBQ0osSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsSUFBSTtRQUNkLFFBQVEsRUFBRTtZQUNOLFNBQVMsRUFBRSxVQUFVLEtBQWE7Z0JBQzlCLE9BQU8sS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNyQixDQUFDO1lBQ0QsT0FBTyxFQUFFLHdDQUF3QztTQUNwRDtLQUNKO0lBQ0QsdUhBQXVIO0lBQ3ZILFNBQVMsRUFBRTtRQUNQLElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFFLElBQUk7UUFDZCxRQUFRLEVBQUU7WUFDTixTQUFTLEVBQUUsVUFBVSxLQUFhO2dCQUM5QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELE9BQU8sRUFBRSwwQkFBMEI7U0FDdEM7S0FDSjtDQUNKLENBQUMsQ0FBQztBQUdVLFFBQUEsSUFBSSxHQUFHLGdCQUFLLENBQVEsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDIn0=