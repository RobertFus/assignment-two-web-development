"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var User = require("../build/persistance/User").User;
var SessionAuth = require('../build/middleware/SessionAuth').SessionAuth;
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var httpMocks = require('node-mocks-http');
var Mongoose = require('mongoose');
//----------------------------------------------------------------------------------------------------------------------
describe('Session Auth Test', function () {
    before(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    beforeEach(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, User.deleteMany({})];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    //---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Getting session ID making sure user is there and authenticated', function () {
            return __awaiter(this, void 0, void 0, function () {
                var request, response, parameters;
                return __generator(this, function (_a) {
                    request = httpsMocks.createRequest({
                        method: 'GET',
                        url: '/user',
                        body: {
                            userName: 'TestuserName',
                            firstName: 'Test',
                            lastName: 'username',
                            password: 'passwordlol',
                            userAddress: '123 Address',
                            userCity: 'CityTest',
                            userState: 'state',
                            userZipcode: 'zipcode',
                            userType: 'admin',
                            userId: 10,
                            sessionId: "0101"
                        }
                    });
                    response = httpMocks.createResponse();
                    parameters = {
                        userName: 'string',
                        firstName: 'string',
                        lastName: 'string',
                        password: 'string',
                        userAddress: 'string',
                        userCity: 'string',
                        userState: 'string',
                        userZipcode: 'string',
                        userType: 'string',
                        userId: 'number',
                        sessionId: 'string'
                    };
                    SessionAuth(parameters, true)(request, response, function () {
                        expect(response.locals.SessionAuth.get('sessionId')).to.equal('0101');
                    });
                    return [2 /*return*/];
                });
            });
        });
    });
    //---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('DELETE SESSION ID. Checking if it is not there.', function () {
            return __awaiter(this, void 0, void 0, function () {
                var request, response, parameters;
                return __generator(this, function (_a) {
                    request = httpsMocks.createRequest({
                        method: 'DELETE',
                        url: '/user',
                        body: {
                            userName: 'TestuserName',
                            firstName: 'Test',
                            lastName: 'username',
                            password: 'passwordlol',
                            userAddress: '123 Address',
                            userCity: 'CityTest',
                            userState: 'state',
                            userZipcode: 'zipcode',
                            userType: 'admin',
                            userId: 10,
                            sessionId: "0101"
                        }
                    });
                    response = httpMocks.createResponse();
                    parameters = {
                        userName: 'string',
                        firstName: 'string',
                        lastName: 'string',
                        password: 'string',
                        userAddress: 'string',
                        userCity: 'string',
                        userState: 'string',
                        userZipcode: 'string',
                        userType: 'string',
                        userId: 'number',
                        sessionId: 'string'
                    };
                    SessionAuth(parameters, true)(request, response, function () {
                        expect(response.locals.SessionAuth.get('sessionId')).to.equal(undefined);
                    });
                    return [2 /*return*/];
                });
            });
        });
    });
    //---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Creating a new User and checking the session ID', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, parameters, savedUser, newSession, userSession, request, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({
                                userName: 'TestuserName',
                                firstName: 'Test',
                                lastName: 'username',
                                password: 'passwordlol',
                                userAddress: '123 Address',
                                userCity: 'CityTest',
                                userState: 'state',
                                userZipcode: 'zipcode',
                                userType: 'admin',
                                userId: 10,
                                sessionId: "0101"
                            })];
                        case 1:
                            user = _a.sent();
                            parameters = {
                                userName: 'string',
                                firstName: 'string',
                                lastName: 'string',
                                password: 'string',
                                userAddress: 'string',
                                userCity: 'string',
                                userState: 'string',
                                userZipcode: 'string',
                                userType: 'string',
                                userId: 'number',
                                sessionId: 'string'
                            };
                            return [4 /*yield*/, user.save()];
                        case 2:
                            savedUser = _a.sent();
                            return [4 /*yield*/, new User({ userId: savedUser })];
                        case 3:
                            newSession = _a.sent();
                            return [4 /*yield*/, newSession.save()];
                        case 4:
                            userSession = _a.sent();
                            request = httpMocks.createRequest({
                                method: 'GET',
                                url: '/user',
                                sessionCookie: userSession.sessionId
                            });
                            response = httpMocks.createResponse();
                            SessionAuth(parameters, true)(request, response, function () {
                                expect(response.locals.sessionId).to.equal(userSession.sessionId);
                            });
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    //---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Using a created user and then deleteing it, then making sure the user is deleted.', function () {
            return __awaiter(this, void 0, void 0, function () {
                var user, parameters, savedUser, userSession, request, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new User({
                                userName: 'TestuserName',
                                firstName: 'Test',
                                lastName: 'username',
                                password: 'passwordlol',
                                userAddress: '123 Address',
                                userCity: 'CityTest',
                                userState: 'state',
                                userZipcode: 'zipcode',
                                userType: 'admin',
                                userId: 10,
                                sessionId: "0101"
                            })];
                        case 1:
                            user = _a.sent();
                            parameters = {
                                userName: 'string',
                                firstName: 'string',
                                lastName: 'string',
                                password: 'string',
                                userAddress: 'string',
                                userCity: 'string',
                                userState: 'string',
                                userZipcode: 'string',
                                userType: 'string',
                                userId: 'number',
                                sessionId: 'string'
                            };
                            return [4 /*yield*/, user.save()];
                        case 2:
                            savedUser = _a.sent();
                            return [4 /*yield*/, parameters.save()];
                        case 3:
                            userSession = _a.sent();
                            request = httpMocks.createRequest({
                                method: 'DELETE',
                                url: '/user',
                                sessionCookie: userSession.sessionId
                            });
                            response = httpMocks.createResponse();
                            SessionAuth(parameters, true)(request, response, function () {
                                expect(response.locals.sessionId).to.equal(userSession.sessionId);
                            });
                            return [2 /*return*/];
                    }
                });
            });
        });
    });
    //---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('There should be no session value to this test', function () {
            var request = httpMocks.createRequest({
                method: 'DELETE',
                url: '/user'
            });
            var response = httpMocks.createResponse();
            var parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, function () {
                expect(response.locals.sessionId).to.equal(undefined);
            });
        });
    });
    //--------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Test to return status code', function () {
            var request = httpMocks.createRequest({
                method: 'DELETE',
                url: '/user',
                sessionCookie: '9999999999999'
            });
            var response = httpMocks.createResponse();
            var parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, function () {
                expect(response.statusCode).to.equal(200);
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbmF1dGhfbWlkZGxld2FyZV90ZXN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3Rlc3Qvc2Vzc2lvbmF1dGhfbWlkZGxld2FyZV90ZXN0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBUSxJQUFBLGdEQUFJLENBQTBDO0FBQzlDLElBQUEsb0VBQVcsQ0FBZ0Q7QUFDbkUsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQzdCLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7QUFDM0IsSUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0FBQy9CLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBQzdDLElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUVyQyx3SEFBd0g7QUFDeEgsUUFBUSxDQUFDLG1CQUFtQixFQUFFO0lBQzFCLE1BQU0sQ0FBQzs7Ozs0QkFDSCxxQkFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLGdDQUFnQyxFQUFFLEVBQUMsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLEVBQUE7O3dCQUFqRixTQUFpRixDQUFDOzs7OztLQUNyRixDQUFDLENBQUM7SUFDSCxVQUFVLENBQUM7Ozs7NEJBQ1AscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBQTs7d0JBQXpCLFNBQXlCLENBQUM7Ozs7O0tBQzdCLENBQUMsQ0FBQztJQUVQLHVIQUF1SDtJQUNuSCxRQUFRLENBQUMsbUJBQW1CLEVBQUU7UUFDMUIsRUFBRSxDQUFDLGdFQUFnRSxFQUFFOzs7O29CQUMzRCxPQUFPLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQzt3QkFDckMsTUFBTSxFQUFFLEtBQUs7d0JBQ2IsR0FBRyxFQUFFLE9BQU87d0JBQ1osSUFBSSxFQUFFOzRCQUNGLFFBQVEsRUFBRSxjQUFjOzRCQUN4QixTQUFTLEVBQUUsTUFBTTs0QkFDakIsUUFBUSxFQUFFLFVBQVU7NEJBQ3BCLFFBQVEsRUFBRSxhQUFhOzRCQUN2QixXQUFXLEVBQUUsYUFBYTs0QkFDMUIsUUFBUSxFQUFFLFVBQVU7NEJBQ3BCLFNBQVMsRUFBRSxPQUFPOzRCQUNsQixXQUFXLEVBQUUsU0FBUzs0QkFDdEIsUUFBUSxFQUFFLE9BQU87NEJBQ2pCLE1BQU0sRUFBRSxFQUFFOzRCQUNWLFNBQVMsRUFBRSxNQUFNO3lCQUNwQjtxQkFDSixDQUFDLENBQUM7b0JBQ0csUUFBUSxHQUFHLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdEMsVUFBVSxHQUFHO3dCQUNmLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixXQUFXLEVBQUUsUUFBUTt3QkFDckIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFNBQVMsRUFBRSxRQUFRO3dCQUNuQixXQUFXLEVBQUUsUUFBUTt3QkFDckIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLE1BQU0sRUFBRSxRQUFRO3dCQUNoQixTQUFTLEVBQUUsUUFBUTtxQkFDdEIsQ0FBQztvQkFDRixXQUFXLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUU7d0JBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUMxRSxDQUFDLENBQUMsQ0FBQzs7OztTQUNOLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRVAsdUhBQXVIO0lBRW5ILFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtRQUMxQixFQUFFLENBQUMsaURBQWlELEVBQUU7Ozs7b0JBQzVDLE9BQU8sR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO3dCQUNyQyxNQUFNLEVBQUUsUUFBUTt3QkFDaEIsR0FBRyxFQUFFLE9BQU87d0JBQ1osSUFBSSxFQUFFOzRCQUNGLFFBQVEsRUFBRSxjQUFjOzRCQUN4QixTQUFTLEVBQUUsTUFBTTs0QkFDakIsUUFBUSxFQUFFLFVBQVU7NEJBQ3BCLFFBQVEsRUFBRSxhQUFhOzRCQUN2QixXQUFXLEVBQUUsYUFBYTs0QkFDMUIsUUFBUSxFQUFFLFVBQVU7NEJBQ3BCLFNBQVMsRUFBRSxPQUFPOzRCQUNsQixXQUFXLEVBQUUsU0FBUzs0QkFDdEIsUUFBUSxFQUFFLE9BQU87NEJBQ2pCLE1BQU0sRUFBRSxFQUFFOzRCQUNWLFNBQVMsRUFBRSxNQUFNO3lCQUNwQjtxQkFDSixDQUFDLENBQUM7b0JBQ0csUUFBUSxHQUFHLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdEMsVUFBVSxHQUFHO3dCQUNmLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFFBQVEsRUFBRSxRQUFRO3dCQUNsQixXQUFXLEVBQUUsUUFBUTt3QkFDckIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFNBQVMsRUFBRSxRQUFRO3dCQUNuQixXQUFXLEVBQUUsUUFBUTt3QkFDckIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLE1BQU0sRUFBRSxRQUFRO3dCQUNoQixTQUFTLEVBQUUsUUFBUTtxQkFDdEIsQ0FBQztvQkFDRixXQUFXLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUU7d0JBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUM3RSxDQUFDLENBQUMsQ0FBQzs7OztTQUNOLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRVAsdUhBQXVIO0lBQ25ILFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtRQUMxQixFQUFFLENBQUMsaURBQWlELEVBQUU7Ozs7O2dDQUNyQyxxQkFBTSxJQUFJLElBQUksQ0FBQztnQ0FDeEIsUUFBUSxFQUFFLGNBQWM7Z0NBQ3hCLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixRQUFRLEVBQUUsVUFBVTtnQ0FDcEIsUUFBUSxFQUFFLGFBQWE7Z0NBQ3ZCLFdBQVcsRUFBRSxhQUFhO2dDQUMxQixRQUFRLEVBQUUsVUFBVTtnQ0FDcEIsU0FBUyxFQUFFLE9BQU87Z0NBQ2xCLFdBQVcsRUFBRSxTQUFTO2dDQUN0QixRQUFRLEVBQUUsT0FBTztnQ0FDakIsTUFBTSxFQUFFLEVBQUU7Z0NBQ1YsU0FBUyxFQUFFLE1BQU07NkJBQ3BCLENBQUMsRUFBQTs7NEJBWkksSUFBSSxHQUFHLFNBWVg7NEJBQ0ksVUFBVSxHQUFHO2dDQUNmLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsUUFBUSxFQUFFLFFBQVE7Z0NBQ2xCLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixXQUFXLEVBQUUsUUFBUTtnQ0FDckIsUUFBUSxFQUFFLFFBQVE7Z0NBQ2xCLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixXQUFXLEVBQUUsUUFBUTtnQ0FDckIsUUFBUSxFQUFFLFFBQVE7Z0NBQ2xCLE1BQU0sRUFBRSxRQUFRO2dDQUNoQixTQUFTLEVBQUUsUUFBUTs2QkFDdEIsQ0FBQzs0QkFDZ0IscUJBQU0sSUFBSSxDQUFDLElBQUksRUFBRSxFQUFBOzs0QkFBN0IsU0FBUyxHQUFHLFNBQWlCOzRCQUNoQixxQkFBTSxJQUFJLElBQUksQ0FBQyxFQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUMsQ0FBQyxFQUFBOzs0QkFBaEQsVUFBVSxHQUFHLFNBQW1DOzRCQUNsQyxxQkFBTSxVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUE7OzRCQUFyQyxXQUFXLEdBQUcsU0FBdUI7NEJBQ3JDLE9BQU8sR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO2dDQUNwQyxNQUFNLEVBQUUsS0FBSztnQ0FDYixHQUFHLEVBQUUsT0FBTztnQ0FDWixhQUFhLEVBQUUsV0FBVyxDQUFDLFNBQVM7NkJBQ3ZDLENBQUMsQ0FBQzs0QkFDRyxRQUFRLEdBQUcsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDOzRCQUM1QyxXQUFXLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUU7Z0NBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDOzRCQUN0RSxDQUFDLENBQUMsQ0FBQzs7Ozs7U0FDTixDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLHVIQUF1SDtJQUVuSCxRQUFRLENBQUMsbUJBQW1CLEVBQUU7UUFDMUIsRUFBRSxDQUFDLG1GQUFtRixFQUFFOzs7OztnQ0FDdkUscUJBQU0sSUFBSSxJQUFJLENBQUM7Z0NBQ3hCLFFBQVEsRUFBRSxjQUFjO2dDQUN4QixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsUUFBUSxFQUFFLFVBQVU7Z0NBQ3BCLFFBQVEsRUFBRSxhQUFhO2dDQUN2QixXQUFXLEVBQUUsYUFBYTtnQ0FDMUIsUUFBUSxFQUFFLFVBQVU7Z0NBQ3BCLFNBQVMsRUFBRSxPQUFPO2dDQUNsQixXQUFXLEVBQUUsU0FBUztnQ0FDdEIsUUFBUSxFQUFFLE9BQU87Z0NBQ2pCLE1BQU0sRUFBRSxFQUFFO2dDQUNWLFNBQVMsRUFBRSxNQUFNOzZCQUNwQixDQUFDLEVBQUE7OzRCQVpJLElBQUksR0FBRyxTQVlYOzRCQUNJLFVBQVUsR0FBRztnQ0FDZixRQUFRLEVBQUUsUUFBUTtnQ0FDbEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixRQUFRLEVBQUUsUUFBUTtnQ0FDbEIsV0FBVyxFQUFFLFFBQVE7Z0NBQ3JCLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsV0FBVyxFQUFFLFFBQVE7Z0NBQ3JCLFFBQVEsRUFBRSxRQUFRO2dDQUNsQixNQUFNLEVBQUUsUUFBUTtnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7NkJBQ3RCLENBQUM7NEJBQ2dCLHFCQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQTs7NEJBQTdCLFNBQVMsR0FBRyxTQUFpQjs0QkFDZixxQkFBTSxVQUFVLENBQUMsSUFBSSxFQUFFLEVBQUE7OzRCQUFyQyxXQUFXLEdBQUcsU0FBdUI7NEJBQ3JDLE9BQU8sR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO2dDQUNwQyxNQUFNLEVBQUUsUUFBUTtnQ0FDaEIsR0FBRyxFQUFFLE9BQU87Z0NBQ1osYUFBYSxFQUFFLFdBQVcsQ0FBQyxTQUFTOzZCQUN2QyxDQUFDLENBQUM7NEJBQ0csUUFBUSxHQUFHLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs0QkFDNUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFO2dDQUM3QyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFDdEUsQ0FBQyxDQUFDLENBQUM7Ozs7O1NBQ04sQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCx1SEFBdUg7SUFFbkgsUUFBUSxDQUFDLG1CQUFtQixFQUFFO1FBQzFCLEVBQUUsQ0FBQywrQ0FBK0MsRUFBRTtZQUNoRCxJQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO2dCQUNwQyxNQUFNLEVBQUUsUUFBUTtnQkFDaEIsR0FBRyxFQUFFLE9BQU87YUFDZixDQUFDLENBQUM7WUFDSCxJQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDNUMsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRO2dCQUNuQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFdBQVcsRUFBRSxRQUFRO2dCQUNyQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFdBQVcsRUFBRSxRQUFRO2dCQUNyQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLFNBQVMsRUFBRSxRQUFRO2FBQ3RCLENBQUM7WUFDRixXQUFXLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUU7Z0JBQzdDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDMUQsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1Asc0hBQXNIO0lBRWxILFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtRQUMxQixFQUFFLENBQUMsNEJBQTRCLEVBQUU7WUFDN0IsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLGFBQWEsQ0FBQztnQkFDcEMsTUFBTSxFQUFFLFFBQVE7Z0JBQ2hCLEdBQUcsRUFBRSxPQUFPO2dCQUNaLGFBQWEsRUFBRSxlQUFlO2FBQ2pDLENBQUMsQ0FBQztZQUNILElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUM1QyxJQUFNLFVBQVUsR0FBRztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsV0FBVyxFQUFFLFFBQVE7Z0JBQ3JCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsV0FBVyxFQUFFLFFBQVE7Z0JBQ3JCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixNQUFNLEVBQUUsUUFBUTtnQkFDaEIsU0FBUyxFQUFFLFFBQVE7YUFDdEIsQ0FBQztZQUNGLFdBQVcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRTtnQkFDN0MsTUFBTSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUMsQ0FBQyxDQUFDIn0=