"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var StrongParams = require('../build/middleware/StrongParams').StrongParams;
var chai = require('chai');
var expect = chai.expect;
var Mongoose = require('mongoose');
var sinon = require('sinon');
var httpMocks = require('node-mocks-http');
//----------------------------------------------------------------------------------------------------------------------
describe('Session Auth Test', function () {
    before(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    beforeEach(function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, User.deleteMany({})];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10
                }
            });
            var response = httpMocks.createResponse();
            var next = sinon.spy();
            var parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should ', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10
                }
            });
            var response = httpMocks.createResponse();
            var parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number'
            };
            StrongParams(parameters, true)(request, response, function () {
                expect(response.locals.strongParams.get('userName')).to.equal('TestuserName');
            });
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/service',
                body: {
                    serviceId: 10,
                    snowService: 'Plow',
                    grassService: 'Mow',
                    lawnService: 'Removal',
                    estimatedCost: number
                }
            });
            var response = httpMocks.createResponse();
            var next = sinon.spy();
            var parameters = {
                serviceId: 'number',
                snowService: 'string',
                grassService: 'string',
                lawnService: 'string',
                estimatedCost: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Status Code Check', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/service',
                body: {
                    serviceId: 10,
                    snowService: 'Plow',
                    grassService: 'Mow',
                    lawnService: 'Removal',
                    estimatedCost: number
                }
            });
            var response = httpMocks.createResponse();
            var next = sinon.spy();
            var parameters = {
                serviceId: 'number',
                snowService: 'string',
                grassService: 'string',
                lawnService: 'string',
                estimatedCost: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(response.statusCode).to.equal(200);
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/review',
                body: {
                    userName: 'UsernameTest',
                    customerComment: 'This is a comment',
                    customerRating: 4,
                    reviewId: 10
                }
            });
            var response = httpMocks.createResponse();
            var next = sinon.spy();
            var parameters = {
                userName: 'string',
                customerComment: 'string',
                customerRating: 'number',
                reviewId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        });
    });
    //----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Making sure that the call has no parameters if there is an incorrect input.', function () {
            var request = httpsMocks.createRequest({
                method: 'POST',
                url: '/review',
                body: {
                    userName: 'UsernameTest',
                    customerComment: 'This is a comment',
                    customerRating: 4,
                    reviewId: 10
                }
            });
            var response = httpMocks.createResponse();
            var next = sinon.spy();
            var parameters = {
                userName: 'string',
                customerComment: 'string',
                customerRating: 'number',
                reviewId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.getCall(0).args[0]).to.not.equal(undefined);
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3Ryb25ncGFyYW1fbWlkZGxld2FyZV90ZXN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3Rlc3Qvc3Ryb25ncGFyYW1fbWlkZGxld2FyZV90ZXN0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBUSxJQUFBLHVFQUFZLENBQWlEO0FBQ3JFLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM3QixJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0FBQzNCLElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztBQUNyQyxJQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDL0IsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUM7QUFFN0Msd0hBQXdIO0FBQ3hILFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtJQUMxQixNQUFNLENBQUM7Ozs7NEJBQ0gscUJBQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxnQ0FBZ0MsRUFBRSxFQUFDLGVBQWUsRUFBRSxJQUFJLEVBQUMsQ0FBQyxFQUFBOzt3QkFBakYsU0FBaUYsQ0FBQzs7Ozs7S0FDckYsQ0FBQyxDQUFDO0lBQ0gsVUFBVSxDQUFDOzs7OzRCQUNQLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUE7O3dCQUF6QixTQUF5QixDQUFDOzs7OztLQUM3QixDQUFDLENBQUM7SUFFUCx3SEFBd0g7SUFDcEgsUUFBUSxDQUFDLDJCQUEyQixFQUFFO1FBQ2xDLEVBQUUsQ0FBQyx5QkFBeUIsRUFBRTtZQUMxQixJQUFNLE9BQU8sR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO2dCQUNyQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsT0FBTztnQkFDWixJQUFJLEVBQUU7b0JBQ0YsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLFNBQVMsRUFBRSxNQUFNO29CQUNqQixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFdBQVcsRUFBRSxhQUFhO29CQUMxQixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsU0FBUyxFQUFFLE9BQU87b0JBQ2xCLFdBQVcsRUFBRSxTQUFTO29CQUN0QixRQUFRLEVBQUUsT0FBTztvQkFDakIsTUFBTSxFQUFFLEVBQUU7aUJBQ2I7YUFDSixDQUFDLENBQUM7WUFDSCxJQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDNUMsSUFBTSxJQUFJLEdBQUcsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLElBQU0sVUFBVSxHQUFHO2dCQUNmLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixXQUFXLEVBQUUsUUFBUTtnQkFDckIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRO2dCQUNuQixXQUFXLEVBQUUsUUFBUTtnQkFDckIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLE1BQU0sRUFBRSxRQUFRO2FBQ25CLENBQUM7WUFDRixZQUFZLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUM7SUFDUCx3SEFBd0g7SUFDcEgsUUFBUSxDQUFDLDJCQUEyQixFQUFFO1FBQ2xDLEVBQUUsQ0FBQyxTQUFTLEVBQUU7WUFDVixJQUFNLE9BQU8sR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO2dCQUNyQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsT0FBTztnQkFDWixJQUFJLEVBQUU7b0JBQ0YsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLFNBQVMsRUFBRSxNQUFNO29CQUNqQixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFdBQVcsRUFBRSxhQUFhO29CQUMxQixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsU0FBUyxFQUFFLE9BQU87b0JBQ2xCLFdBQVcsRUFBRSxTQUFTO29CQUN0QixRQUFRLEVBQUUsT0FBTztvQkFDakIsTUFBTSxFQUFFLEVBQUU7aUJBQ2I7YUFDSixDQUFDLENBQUM7WUFDSCxJQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDNUMsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFNBQVMsRUFBRSxRQUFRO2dCQUNuQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFdBQVcsRUFBRSxRQUFRO2dCQUNyQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFdBQVcsRUFBRSxRQUFRO2dCQUNyQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsTUFBTSxFQUFFLFFBQVE7YUFDbkIsQ0FBQztZQUNGLFlBQVksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRTtnQkFDOUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDbEYsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFDO0lBRVAsd0hBQXdIO0lBQ3BILFFBQVEsQ0FBQywyQkFBMkIsRUFBRTtRQUNsQyxFQUFFLENBQUMseUJBQXlCLEVBQUU7WUFDMUIsSUFBTSxPQUFPLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQztnQkFDckMsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsR0FBRyxFQUFFLFVBQVU7Z0JBQ2YsSUFBSSxFQUFFO29CQUNGLFNBQVMsRUFBRSxFQUFFO29CQUNiLFdBQVcsRUFBRSxNQUFNO29CQUNuQixZQUFZLEVBQUUsS0FBSztvQkFDbkIsV0FBVyxFQUFFLFNBQVM7b0JBQ3RCLGFBQWEsRUFBRSxNQUFNO2lCQUN4QjthQUNKLENBQUMsQ0FBQztZQUNILElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUM1QyxJQUFNLElBQUksR0FBRyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLFdBQVcsRUFBRSxRQUFRO2dCQUNyQixZQUFZLEVBQUUsUUFBUTtnQkFDdEIsV0FBVyxFQUFFLFFBQVE7Z0JBQ3JCLGFBQWEsRUFBRSxRQUFRO2FBQzFCLENBQUM7WUFDRixZQUFZLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQyxDQUFDLENBQUM7SUFFUCx3SEFBd0g7SUFDcEgsUUFBUSxDQUFDLDJCQUEyQixFQUFFO1FBQ2xDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRTtZQUNwQixJQUFNLE9BQU8sR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO2dCQUNyQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxHQUFHLEVBQUUsVUFBVTtnQkFDZixJQUFJLEVBQUU7b0JBQ0YsU0FBUyxFQUFFLEVBQUU7b0JBQ2IsV0FBVyxFQUFFLE1BQU07b0JBQ25CLFlBQVksRUFBRSxLQUFLO29CQUNuQixXQUFXLEVBQUUsU0FBUztvQkFDdEIsYUFBYSxFQUFFLE1BQU07aUJBQ3hCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsSUFBTSxRQUFRLEdBQUcsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzVDLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFNLFVBQVUsR0FBRztnQkFDZixTQUFTLEVBQUUsUUFBUTtnQkFDbkIsV0FBVyxFQUFFLFFBQVE7Z0JBQ3JCLFlBQVksRUFBRSxRQUFRO2dCQUN0QixXQUFXLEVBQUUsUUFBUTtnQkFDckIsYUFBYSxFQUFFLFFBQVE7YUFDMUIsQ0FBQztZQUNGLFlBQVksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN4RCxNQUFNLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVQLHdIQUF3SDtJQUNwSCxRQUFRLENBQUMsMkJBQTJCLEVBQUU7UUFDbEMsRUFBRSxDQUFDLHlCQUF5QixFQUFFO1lBQzFCLElBQU0sT0FBTyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUM7Z0JBQ3JDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLEdBQUcsRUFBRSxTQUFTO2dCQUNkLElBQUksRUFBRTtvQkFDRixRQUFRLEVBQUUsY0FBYztvQkFDeEIsZUFBZSxFQUFFLG1CQUFtQjtvQkFDcEMsY0FBYyxFQUFFLENBQUM7b0JBQ2pCLFFBQVEsRUFBRSxFQUFFO2lCQUNmO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsSUFBTSxRQUFRLEdBQUcsU0FBUyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzVDLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFNLFVBQVUsR0FBRztnQkFDZixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsZUFBZSxFQUFFLFFBQVE7Z0JBQ3pCLGNBQWMsRUFBRSxRQUFRO2dCQUN4QixRQUFRLEVBQUUsUUFBUTthQUNyQixDQUFDO1lBQ0YsWUFBWSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1Asd0hBQXdIO0lBQ3BILFFBQVEsQ0FBQywyQkFBMkIsRUFBRTtRQUNsQyxFQUFFLENBQUMsNkVBQTZFLEVBQUU7WUFDOUUsSUFBTSxPQUFPLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQztnQkFDckMsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsR0FBRyxFQUFFLFNBQVM7Z0JBQ2QsSUFBSSxFQUFFO29CQUNGLFFBQVEsRUFBRSxjQUFjO29CQUN4QixlQUFlLEVBQUUsbUJBQW1CO29CQUNwQyxjQUFjLEVBQUUsQ0FBQztvQkFDakIsUUFBUSxFQUFFLEVBQUU7aUJBQ2Y7YUFDSixDQUFDLENBQUM7WUFDSCxJQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDNUMsSUFBTSxJQUFJLEdBQUcsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLElBQU0sVUFBVSxHQUFHO2dCQUNmLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixlQUFlLEVBQUUsUUFBUTtnQkFDekIsY0FBYyxFQUFFLFFBQVE7Z0JBQ3hCLFFBQVEsRUFBRSxRQUFRO2FBQ3JCLENBQUM7WUFDRixZQUFZLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDeEQsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUQsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDLENBQUMsQ0FBQztBQUNQLENBQUMsQ0FBQyxDQUFDIn0=