import Express, {request, Request, Response} from 'express';
import Mongoose from 'mongoose';
import {User} from "./persistance/User";
import {strongParams} from "./middleware/StrongParams";
import {ServiceTypes} from "./persistance/ServiceType";
import {ReviewContactor} from "./persistance/ReviewContractor";
import cookieParser from 'cookie-parser';
import {SessionAuth} from "./middleware/SessionAuth";
import graphqlHTTP from "express-graphql";
import GraphQlSchema from "./graph-ql/GraphQlSchema";
import WebSocket from 'ws';
const app = Express();
const port: number = 5000;


app.use(Express.json());
app.use(cookieParser('Secret'));
Mongoose.connect('mongodb://localhost/test');

app.post('/user' , [
    strongParams( { // Strong parameters middleware
        userName: 'string',
        firstName: 'string',
        lastName: 'string',
        password: 'string',
        userAddress: 'string',
        userCity: 'string',
        userState: 'string',
        userZipcode: 'string',
        userType: 'string',
        userId: 'number',
        sessionId: 'string'
    })
] , async (request: Request, response: Response) => {
    const strongParams: any = response.locals.strongParams;

    const userName: string | undefined = strongParams.userName; //For Some reason the elvis operator comes up as error
    const firstName: string | undefined = strongParams.firstName;
    const lastName: string | undefined = strongParams.lastName;
    const password: string | undefined = strongParams.password;
    const userAddress: string | undefined = strongParams.userAddress;
    const userCity: string | undefined = strongParams.userCity;
    const userState: string | undefined = strongParams.userState;
    const userZipcode: string | undefined = strongParams.userZipcode;
    const userType: string | undefined = strongParams.userType;
    const userId: number | undefined = strongParams.userId;
    const sessionId: string | undefined = strongParams.sessionId;

    const user = await User.create( {
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        password: password,
        userAddress: userAddress,
        userCity: userCity,
        userState: userState,
        userZipcode: userZipcode,
        userType: userType,
        userId: userId,
        sessionId: sessionId
    });
    response.send(user);
    });


//Get requests for user, Searches for one user.
//----------------------------------------------------------------------------------------------------------------------
app.get('/user', [SessionAuth({requireCookie: true})] , async (request: Request, response: Response) => {

    const userName: string | undefined = request.body.userName; //For Some reason the elvis operator comes up as error
    const firstName: string | undefined = request.body.firstName;
    const lastName: string | undefined = request.body.lastName;
    const password: string | undefined = request.body.password;
    const userAddress: string | undefined = request.body.userAddress;
    const userCity: string | undefined = request.body.userCity;
    const userState: string | undefined = request.body.userState;
    const userZipcode: string | undefined = request.body.userZipcode;
    const userType: string | undefined = request.body.userType;
    const userId: number | undefined = request.body.userId;
    const sessionId: string | undefined = request.body.sessionId;

    const user = await User.findOne( {
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        password: password,
        userAddress: userAddress,
        userCity: userCity,
        userState: userState,
        userZipcode: userZipcode,
        userType: userType,
        userId: userId,
        sessionId: sessionId
    });
    return response.send(user);

});

//Delete request for users, will delete one user when requested
//----------------------------------------------------------------------------------------------------------------------
app.delete('/user', [SessionAuth({requireCookie: true})] , async (request: Request, response: Response) => {

    const userName: string | undefined = request.body.userName; //For Some reason the elvis operator comes up as error
    const firstName: string | undefined = request.body.firstName;
    const lastName: string | undefined = request.body.lastName;
    const password: string | undefined = request.body.password;
    const userAddress: string | undefined = request.body.userAddress;
    const userCity: string | undefined = request.body.userCity;
    const userState: string | undefined = request.body.userState;
    const userZipcode: string | undefined = request.body.userZipcode;
    const userType: string | undefined = request.body.userType;
    const userId: number | undefined = request.body.userId;
    const sessionId: string | undefined = request.body.sessionId;

    const user = await User.deleteOne( {
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        password: password,
        userAddress: userAddress,
        userCity: userCity,
        userState: userState,
        userZipcode: userZipcode,
        userType: userType,
        userId: userId,
        sessionId: sessionId
    });
    return response.send(user);
});

//----------------------------------------------------------------------------------------------------------------------
app.post('/service',
    [ strongParams( {
        serviceId: 'number',
        snowService: 'string',
        grassService: 'string',
        lawnService: 'string',
        estimatedCost: 'number'

    })] , async (request: Request, response: Response) => {

        const strongParams: any = response.locals.strongParams;

        const serviceId: number | undefined = strongParams.serviceId;
        const snowService: string | undefined = strongParams.snowService;
        const grassService: string | undefined = strongParams.grassService;
        const lawnService: string | undefined = strongParams.lawnService;
        const estimatedCost: number | undefined = strongParams.estimatedCost;
        const service = await ServiceTypes.create({
            serviceId: serviceId,
            snowService: snowService ,
            grassService: grassService,
            lawnService: lawnService,
            estimatedCost: estimatedCost
        });
        response.send(service);

    });

//----------------------------------------------------------------------------------------------------------------------
app.get('/service', async (request: Request, response: Response) => {
    const serviceId: number | undefined = request.body.serviceId;
    const snowService: string | undefined = request.body.snowService;
    const grassService: string | undefined = request.body.grassService;
    const lawnService: string | undefined = request.body.lawnService;
    const estimatedCost: number | undefined = request.body.estimatedCost;
    const service = await ServiceTypes.findOne({
        serviceId: serviceId,
        snowService: snowService,
        grassService: grassService,
        lawnService: lawnService,
        estimatedCost: estimatedCost
    });
    return response.send(service);
});

//----------------------------------------------------------------------------------------------------------------------
app.delete('/service', async (request: Request, response: Response) => {
    const serviceId: number | undefined = request.body.serviceId;
    const snowService: string | undefined = request.body.snowService;
    const grassService: string | undefined = request.body.grassService;
    const lawnService: string | undefined = request.body.lawnService;
    const estimatedCost: number | undefined = request.body.estimatedCost;
    const service = await ServiceTypes.deleteOne({
        serviceId: serviceId,
        snowService: snowService,
        grassService: grassService,
        lawnService: lawnService,
        estimatedCost: estimatedCost

    });
    return response.send(service);
});
//Post request for reviews
//----------------------------------------------------------------------------------------------------------------------
app.post('/review',
    [strongParams( {
        userName: 'string',
        customerComment: 'string',
        customerRating: 'number',
        reviewId: 'number'
    })] , async (request: Request, response: Response) => {

        const strongParams: any = response.locals.strongParams;
        const userName: string | undefined = strongParams.userName;
        const customerComment: string | undefined = strongParams.customerComment;
        const customerRating: number | undefined = strongParams.customerRating;
        const reviewId: number | undefined = strongParams.reviewId;
        const review = await ReviewContactor.create({
            userName: userName,
            customerComment: customerComment,
            customerRating: customerRating,
            reviewId: reviewId
        });
        response.send(review);
    });
//Get request for reviews.
//----------------------------------------------------------------------------------------------------------------------
app.get('/review', async (request: Request, response: Response) => {

    const userName: string | undefined = request.body.userName;
    const customerComment: string | undefined = request.body.customerComment;
    const customerRating: number | undefined = request.body.customerRating;
    const reviewId: number | undefined = request.body.reviewId;
    const review = await ReviewContactor.findOne({
        userName: userName,
        customerComment: customerComment,
        customerRating: customerRating,
        reviewId: reviewId
    });
    return response.send(review);

});
//Delete request for review contractor model.
//----------------------------------------------------------------------------------------------------------------------
app.delete('/review', async (request: Request, response: Response) => {

    const userName: string | undefined = request.body.userName;
    const customerComment: string | undefined = request.body.customerComment;
    const customerRating: number | undefined = request.body.customerRating;
    const reviewId: number | undefined = request.body.reviewId;
    const review = await ReviewContactor.deleteOne({
        userName: userName,
        customerComment: customerComment,
        customerRating: customerRating,
        reviewId: reviewId
    });
    return response.send(review);
});

app.use('/graphql', graphqlHTTP({
    schema: GraphQlSchema,
    graphiql: true

}));

/*
const webSocketServer: WebSocket.Server = new WebSocket.Server({ port });
webSocketServer.on('connection', (webSocketClient: WebSocket) => {
    console.log('A client has connected');
    webSocketClient.send('You have successfully connected!');
    webSocketClient.on('message', (message) => {
        console.log('Received message from client: ', message);
        webSocketServer.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
                client.send('Broadcasted message ' + message);


            }
        });
        webSocketClient.on('message', (message) => {
            console.log('received %s', message);
        });
        console.log('Broadcasted message to everyone');
    });
});

console.log('Websocket server is up', port);

 */


app.listen(4000, () => console.log('Server Up'));