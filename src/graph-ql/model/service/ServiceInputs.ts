import {GraphQLInputObjectType, GraphQLString} from "graphql";

export const ServiceInput = new GraphQLInputObjectType({
    name: 'Service',
    description: 'Description of a service',
    fields: () => ({
        serviceId: { type: GraphQLString },
        snowService: { type: GraphQLString },
        grassService: { type: GraphQLString },
        lawnService: { type: GraphQLString },
        estimatedCost: { type: GraphQLString },
    })
});
