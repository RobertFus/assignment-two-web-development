import {GraphQLList, GraphQLObjectType, GraphQLString} from "graphql";
import {ServiceTypes} from "../../../persistance/ServiceType";

export const GraphQlService: GraphQLObjectType<any, any> = new GraphQLObjectType({
    name: 'ServiceInput',
    description: 'Description of a service',
    fields: () => ({
        serviceId: { type: GraphQLString },
        snowService: { type: GraphQLString },
        grassService: { type: GraphQLString },
        lawnService: { type: GraphQLString },

        //I think this is right to make this a child of lawn service?
        estimatedCost: {
            type: GraphQLList(GraphQlService),
            resolve(parent, args) {
                return ServiceTypes.find({estimatedCost: {$in: parent.lawnService}});
            }
        }
    })
});
