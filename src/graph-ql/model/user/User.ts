import {GraphQLObjectType, GraphQLString} from "graphql";

export const GraphQlUser = new GraphQLObjectType({
    name: 'User',
    description: 'User Account',
    fields: () => ({
        userName: { type: GraphQLString },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        password: { type: GraphQLString },
        userAddress: { type: GraphQLString },
        userCity: { type: GraphQLString },
        userState: { type: GraphQLString },
        userZipcode: { type: GraphQLString },
        userType: { type: GraphQLString },
        userId: { type: GraphQLString },
        sessionId: { type: GraphQLString },
    })
});
