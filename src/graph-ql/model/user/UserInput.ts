import {GraphQLInputObjectType, GraphQLString} from "graphql";

export const UserInput = new GraphQLInputObjectType({

    name: 'UserInput',
    description: 'Input fields for the User model',

    fields: () => ({
        userName: { type: GraphQLString },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        password: { type: GraphQLString },
        userAddress: { type: GraphQLString },
        userCity: { type: GraphQLString },
        userState: { type: GraphQLString },
        userZipcode: { type: GraphQLString },
        userType: { type: GraphQLString },
        userId: { type: GraphQLString },
        sessionId: { type: GraphQLString },
    }),
});