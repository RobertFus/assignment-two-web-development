import {GraphQLObjectType} from "graphql";
import {User} from "../../persistance/User";
import {ServiceTypes} from "../../persistance/ServiceType";
import {GraphQlUser} from "../model/user/User";
import {UserInput} from "../model/user/UserInput";
import { GraphQlService } from "../model/service/ServiceTypes";
import { ServiceInput } from "../model/service/ServiceInputs";

export default new GraphQLObjectType({

    name: 'RootMutations',
    description: 'Root level mutator resolvers',

    fields: () => ({
        createUser: {
            type: GraphQlUser,
            description: 'Creates a new user',
            args: {
                input: { type: UserInput }
            },
            resolve: (parentValue: any, {input : { userName, firstName, lastName, password, userAddress, userCity, userState, userZipcode, userType, userId, sessionId} }) => {
                return User.create({ userName, firstName, lastName, password, userAddress, userCity, userState, userZipcode, userType, userId, sessionId });

            },
        },
        createService: {
            type: GraphQlService,
            description: 'Creates service inputs',
            args: {
                input: { type: ServiceInput }
            },
            resolve: (parentValue: any, {input : {serviceId, snowService, grassService, lawnService, estimatedCost } }) => {
                return ServiceTypes.create({serviceId, snowService, grassService, lawnService, estimatedCost });

            },
        },

    })
});