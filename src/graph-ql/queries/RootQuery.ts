import { GraphQLList, GraphQLObjectType} from "graphql";
import { User } from "../../persistance/User";
import {ServiceTypes} from "../../persistance/ServiceType";

import { GraphQlUser } from "../model/user/User";
import {GraphQlService} from "../model/service/ServiceTypes";

export default new GraphQLObjectType({
    name: 'RootQuery',
    description: 'Root level query resolvers',
    fields: () => ({
        users: {
            type: new GraphQLList(GraphQlUser),
            resolve(parentValue, args) {
                return User.findOne ({ $or: [{ userName: args.userName }]});
            }
        },
        services: {
            type: new GraphQLList(GraphQlService),
            resolve(parentValue, args) {
                return ServiceTypes.find({});
            }
        }
    })
});