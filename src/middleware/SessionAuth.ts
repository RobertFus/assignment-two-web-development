import { Request, Response, NextFunction, RequestHandler } from 'express';
import { User } from '../persistance/User'
//Session Authentication to make sure that the user that is signed in is the current User
export function SessionAuth({ requireCookie = false}: {requireCookie: boolean}) : RequestHandler {
    return(request: Request, response: Response, next: NextFunction) => {
    if(request.signedCookies.sessionId) {
        const sessionId: string = request.signedCookies.sessionId;
        const currentSession = User.findOne({
            sessionId: sessionId
        });
        //Checks the current sessoin and sees if there is a session ID connected, if there is a session ID connected,
        // it moves forwards
        if(currentSession) {
            response.locals.sessionId = sessionId;
            return next();
        }
    }
    //Checks to cookie and if there is no cookie attached, it will send a 400 status
    if(requireCookie) {
        response.sendStatus(400);
        return next();
    }
    }
}
