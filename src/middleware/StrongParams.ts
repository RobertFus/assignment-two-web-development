import {NextFunction, Request, Response} from 'express';

export function strongParams(params: { [key: string ] : string }) {
    return (request: Request, response: Response, next: NextFunction) => {
        const strongParams: { [key: string ]: any } = {}
        const weakParams = request.body;

        Object.entries(params).forEach(([weakParamKey, specifiedType]) => {
            const weakParam = weakParams[weakParamKey];
            if(weakParam && typeof weakParam === specifiedType) {
                strongParams[weakParamKey] = weakParam
            }
        });

        response.locals.strongParams = strongParams;
        request.body = null;
        return next();
    }
}