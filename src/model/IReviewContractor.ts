import { Document } from 'mongoose';

/*
* This is a model for the review class, This is built so that the user has the ability to review the contractor that does
* the work for them.
 */
export interface IReviewContractor extends Document {

    userName: string,
    customerComment: string,
    customerRating: number,
    reviewId: number

}
