import {Schema, model} from 'mongoose';
import {IServiceTypes} from "../model/IServiceType";

//---------------------------------------------------------------------------------------------------------------------
const serviceSchema: Schema<IServiceTypes> = new Schema<IServiceTypes> ({
    serviceId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value: number): boolean {
                return value > 0;
            },
            message: 'The id must be greater than 0'
        }
    },
//---------------------------------------------------------------------------------------------------------------------
    snowService: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    grassService: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    lawnService: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value) && value.length < 300;
            },
            message: 'The service must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    estimatedCost: {
        type: Number,
        required: true,
        validate: {
            validator: function (value: number): boolean {
                return value > 0;
            },
            message: 'The service cost must be greater than 0'
        }
    }

});


export const ServiceTypes = model<IServiceTypes>('service', serviceSchema);