import {Schema, model} from 'mongoose';
import { IUser } from "../model/IUser";

const userSchema: Schema<IUser> = new Schema<IUser>({

//---------------------------------------------------------------------------------------------------------------------
    userName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 6;
            },
            message: 'User name must contain more than 6 characters'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name must be a character'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Last Name must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    password: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z0-9!@#$%^&*]*$/.test(value);
            },
            message: 'Password must meet criteria a-z A-Z 0-9 !@#$%^&*'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userAddress: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
        },
        message: 'User address must be larger than 0'
    },

//---------------------------------------------------------------------------------------------------------------------
    userCity: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'City Must be a letter'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userState: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'State must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userZipcode: {
        type: String,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Zipcode must be 5 digits'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userType: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Type must be a character'
        }
    },

//---------------------------------------------------------------------------------------------------------------------
    userId: {
        type: Number,
        required: true,
        validate: {
            validator: function (value: number): boolean {
                return value > 0;
            },
            message: 'Id must be a number greater than zero.'
        }
    },
    //---------------------------------------------------------------------------------------------------------------------
    sessionId: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Type must be a character'
        }
    }
});


export const User = model<IUser>('user', userSchema);