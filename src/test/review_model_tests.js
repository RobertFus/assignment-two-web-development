/*
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const { ReviewContractor } = require('../build/persistance/ReviewContractor');

describe('Review model tests', function() {

    before(async function(){
        await Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
    });

    beforeEach(async function() {
        await ReviewContractor.deleteMany({});
    });
//Test to see if rating system works properly
//---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function() {
        it('Allow for rating to be between 1-5', async function() {
            const review = await new ReviewContractor({ customerRating: 'RATING' });
            expect(review.customerRating).to.equal('RATING');
            review.customerRating = '5';
            expect(review.customerRating).to.equal('5');
            const savedUser = await review.save();
            expect(savedUser.customerRating).to.equal('5');
        });
    });
//Test to prevent rating to go above 5
//---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function() {
        it('Prevent Rating to be above 5', async function() {
            const review = await new ReviewContractor({ customerRating: '10' });
            try {
                await review.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Review validation failed: Rating Above 5');
            }
        });
    });
//Test to prevent Rating to be a letter
//---------------------------------------------------------------------------------------------------------------------
    describe('Rating Validation Test', function() {
        it('Prevent Rating to be a letter', async function() {
            const review = await new ReviewContractor({ customerRating: 'ABC' });
            try {
                await review.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Review validation failed: Rating can not be a letter');
            }
        });
    });
// Comment validation test to make sure that the characters stay within 300 characters to prevent large build up
//---------------------------------------------------------------------------------------------------------------------
    describe('Comment Validation Test', function() {
        it('Comment Must be inbetween 300 characters', async function() {
            const review = await new ReviewContractor({ customerComment: 'Comment' });
            expect(review.customerComment).to.equal('Comment');
            review.customerComment = 'THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS';
            expect(review.customerComment).to.equal('THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS');
            const savedUser = await review.save();
            expect(savedUser.customerComment).to.equal('THIS IS A COMMENT TO MAKE SURE IT IS UNDER 300 CHARACTERS');
        });
    });
// ID Validation to prevent ID To have a letter.
//---------------------------------------------------------------------------------------------------------------------
    describe('ID Validation Test', function() {
        it('Prevent ID to be a letter', async function() {
            const review = await new ReviewContractor({ customerRating: 'ABC15325' });
            try {
                await review.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('Review validation failed: Rating can not be a letter');
            }
        });
    });

});
*/
