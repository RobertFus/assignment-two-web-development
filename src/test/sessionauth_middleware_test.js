const { User } = require("../build/persistance/User");
const { SessionAuth } = require('../build/middleware/SessionAuth');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const httpMocks = require('node-mocks-http');
const Mongoose = require('mongoose');

//----------------------------------------------------------------------------------------------------------------------
describe('Session Auth Test', function () {
    before(async function () {
        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });
    beforeEach(async function () {
        await User.deleteMany({});
    });

//---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Getting session ID making sure user is there and authenticated', async function () {
            const request = httpsMocks.createRequest({
                method: 'GET',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10,
                    sessionId: "0101"
                }
            });
            const response = httpMocks.createResponse();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.locals.SessionAuth.get('sessionId')).to.equal('0101');
            });
        });
    });

//---------------------------------------------------------------------------------------------------------------------

    describe('Session Auth Test', function () {
        it('DELETE SESSION ID. Checking if it is not there.', async function () {
            const request = httpsMocks.createRequest({
                method: 'DELETE',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10,
                    sessionId: "0101"
                }
            });
            const response = httpMocks.createResponse();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.locals.SessionAuth.get('sessionId')).to.equal(undefined);
            });
        });
    });

//---------------------------------------------------------------------------------------------------------------------
    describe('Session Auth Test', function () {
        it('Creating a new User and checking the session ID', async function () {
            const user = await new User({
                userName: 'TestuserName',
                firstName: 'Test',
                lastName: 'username',
                password: 'passwordlol',
                userAddress: '123 Address',
                userCity: 'CityTest',
                userState: 'state',
                userZipcode: 'zipcode',
                userType: 'admin',
                userId: 10,
                sessionId: "0101"
            });
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            const savedUser = await user.save();
            const newSession = await new User({userId: savedUser});
            const userSession = await newSession.save();
            const request = httpMocks.createRequest({
                method: 'GET',
                url: '/user',
                sessionCookie: userSession.sessionId
            });
            const response = httpMocks.createResponse();
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.locals.sessionId).to.equal(userSession.sessionId);
            });
        });
    });
//---------------------------------------------------------------------------------------------------------------------

    describe('Session Auth Test', function () {
        it('Using a created user and then deleteing it, then making sure the user is deleted.', async function () {
            const user = await new User({
                userName: 'TestuserName',
                firstName: 'Test',
                lastName: 'username',
                password: 'passwordlol',
                userAddress: '123 Address',
                userCity: 'CityTest',
                userState: 'state',
                userZipcode: 'zipcode',
                userType: 'admin',
                userId: 10,
                sessionId: "0101"
            });
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            const savedUser = await user.save();
            const userSession = await parameters.save();
            const request = httpMocks.createRequest({
                method: 'DELETE',
                url: '/user',
                sessionCookie: userSession.sessionId
            });
            const response = httpMocks.createResponse();
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.locals.sessionId).to.equal(userSession.sessionId);
            });
        });
    });
//---------------------------------------------------------------------------------------------------------------------

    describe('Session Auth Test', function () {
        it('There should be no session value to this test', function () {
            const request = httpMocks.createRequest({
                method: 'DELETE',
                url: '/user'
            });
            const response = httpMocks.createResponse();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.locals.sessionId).to.equal(undefined);
            });
        });
    });
//--------------------------------------------------------------------------------------------------------------------

    describe('Session Auth Test', function () {
        it('Test to return status code', function () {
            const request = httpMocks.createRequest({
                method: 'DELETE',
                url: '/user',
                sessionCookie: '9999999999999'
            });
            const response = httpMocks.createResponse();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number',
                sessionId: 'string'
            };
            SessionAuth(parameters, true)(request, response, () => {
                expect(response.statusCode).to.equal(200)
            });
        });
    });
});