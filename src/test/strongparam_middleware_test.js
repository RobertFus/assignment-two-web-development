const { StrongParams } = require('../build/middleware/StrongParams');
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const sinon = require('sinon');
const httpMocks = require('node-mocks-http');

//----------------------------------------------------------------------------------------------------------------------
describe('Session Auth Test', function () {
    before(async function () {
        await Mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
    });
    beforeEach(async function () {
        await User.deleteMany({});
    });

//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10
                }
            });
            const response = httpMocks.createResponse();
            const next = sinon.spy();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        })
    });
//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should ', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/user',
                body: {
                    userName: 'TestuserName',
                    firstName: 'Test',
                    lastName: 'username',
                    password: 'passwordlol',
                    userAddress: '123 Address',
                    userCity: 'CityTest',
                    userState: 'state',
                    userZipcode: 'zipcode',
                    userType: 'admin',
                    userId: 10
                }
            });
            const response = httpMocks.createResponse();
            const parameters = {
                userName: 'string',
                firstName: 'string',
                lastName: 'string',
                password: 'string',
                userAddress: 'string',
                userCity: 'string',
                userState: 'string',
                userZipcode: 'string',
                userType: 'string',
                userId: 'number'
            };
            StrongParams(parameters, true)(request, response, () => {
                expect(response.locals.strongParams.get('userName')).to.equal('TestuserName');
            })
        })
    });

//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/service',
                body: {
                    serviceId: 10,
                    snowService: 'Plow',
                    grassService: 'Mow',
                    lawnService: 'Removal',
                    estimatedCost: number
                }
            });
            const response = httpMocks.createResponse();
            const next = sinon.spy();
            const parameters = {
                serviceId: 'number',
                snowService: 'string',
                grassService: 'string',
                lawnService: 'string',
                estimatedCost: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        })
    });

//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Status Code Check', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/service',
                body: {
                    serviceId: 10,
                    snowService: 'Plow',
                    grassService: 'Mow',
                    lawnService: 'Removal',
                    estimatedCost: number
                }
            });
            const response = httpMocks.createResponse();
            const next = sinon.spy();
            const parameters = {
                serviceId: 'number',
                snowService: 'string',
                grassService: 'string',
                lawnService: 'string',
                estimatedCost: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(response.statusCode).to.equal(200);
        });
    });

//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Should Called Next Once', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/review',
                body: {
                    userName: 'UsernameTest',
                    customerComment: 'This is a comment',
                    customerRating: 4,
                    reviewId: 10
                }
            });
            const response = httpMocks.createResponse();
            const next = sinon.spy();
            const parameters = {
                userName: 'string',
                customerComment: 'string',
                customerRating: 'number',
                reviewId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.calledOnce).to.equal(true);
        })
    });
//----------------------------------------------------------------------------------------------------------------------
    describe('Strong Param Unit Testing', function () {
        it('Making sure that the call has no parameters if there is an incorrect input.', function () {
            const request = httpsMocks.createRequest({
                method: 'POST',
                url: '/review',
                body: {
                    userName: 'UsernameTest',
                    customerComment: 'This is a comment',
                    customerRating: 4,
                    reviewId: 10
                }
            });
            const response = httpMocks.createResponse();
            const next = sinon.spy();
            const parameters = {
                userName: 'string',
                customerComment: 'string',
                customerRating: 'number',
                reviewId: 'number'
            };
            StrongParams(parameters, true)(request, response, next);
            expect(next.getCall(0).args[0]).to.not.equal(undefined);
        })
    });
});