/*
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const { User } = require('../build/persistance/User');

//---------------------------------------------------------------------------------------------------------------------
describe('User model tests', function() {

    before(async function(){
        await Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
    });

    beforeEach(async function() {
        await User.deleteMany({});
    });
// Make address into a full address.
//---------------------------------------------------------------------------------------------------------------------
    describe('Full Address  virtual test', function() {
        it('Allow for full address to be set virtual', async function() {
            const user = await new User({ userAddress: 'ADDRESS', userCity: 'CITY', userState: 'STATE' });
            expect(user.userAddress).to.equal('ADDRESS');
            expect(user.userCity).to.equal('CITY');
            expect(user.userState).to.equal('STATE');

            user.fullAddress = '300 Boston Post Rd-West Haven-Connecticut'
            expect(user.userAddress).to.equal('300 Boston Post Rd');
            expect(user.userCity).to.equal('West Haven');
            expect(user.userState).to.equal('Connecticut');

            const savedUser = await user.save();
            expect(savedUser.userAddress).to.equal('300 Boston Post Rd');
            expect(savedUser.userCity).to.equal('West Haven');
            expect(savedUser.userState).to.equal('Connecticut');
        });
    });
// Zipcode validation to make sure zipcode is within 5 digits in length
//---------------------------------------------------------------------------------------------------------------------
    describe('Zipcode validation test', function() {
        it('Allow for zipcode to be 5 numbers', async function() {
            const user = await new User({ userZipcode: 'ZIPCODE' });
            expect(user.userZipcode).to.equal('ZIPCODE');
            user.userZipcode = '06418';
            expect(user.userZipcode).to.equal('06418');
            const savedUser = await user.save();
            expect(savedUser.userZipcode).to.equal('06418');
        });
    });
//Allows for the user type to have any alphabetic character
//---------------------------------------------------------------------------------------------------------------------
    describe( 'User Type validation test', function() {
        it('Allow for user type or be any alphabetic character', async function() {
            const user = await new User({ userType: 'ADMIN_TYPE' });
            expect(user.userType).to.equal('ADMIN_TYPE');
            user.userType = 'Admin';
            expect(user.userType).to.equal('Admin');
            const savedUser = await user.save();
            expect(savedUser.userType).to.equal('Admin');
        });
    });
//Prevent to username to have under 6 characters
//---------------------------------------------------------------------------------------------------------------------
    describe('User name validation test', function() {
        it('Prevent user names under 6 characters', async function() {
            const user = await new User({ userName: 'John' });
            try {
                await user.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('user validation failed: User name under 6 Characters');
            }
        });
    });
//Prevent the last name of having numbers
//---------------------------------------------------------------------------------------------------------------------
    describe('last name validation test', function() {
        it('Prevent last name to have numbers', async function() {
            const user = await new User({ lastName: 'John123' });
            try {
                await user.save();
                expect.fail("Expected error not thrown");
            } catch(error) {
                expect(error.message).to.equal('user validation failed: Last Name has numbers, Not allowed');
            }
        });
    });
});

 */